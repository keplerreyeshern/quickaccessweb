import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
    sessionStorage.setItem('url_global', 'https://backend.quickaccess.mx/api');
    sessionStorage.setItem('url_images', 'https://backend.quickaccess.mx/storage/images');
  }

  postToken(params){
    return this.http.post<any>('https://backend.quickaccess.mx/oauth/token', params);
  }

  getDataUser(url, params){
    const headers = new HttpHeaders({
      'Authorization': params,
    });
    return this.http.get<any>(url + '/profile', {headers});
  }
}
