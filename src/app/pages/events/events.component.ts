import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {faEye} from '@fortawesome/free-regular-svg-icons';
import {faSearch} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.sass']
})
export class EventsComponent implements OnInit {

  faEye = faEye;
  faSearch = faSearch;
  eventsAll:any[]=[];
  events:any[]=[];
  complexes:any[]=[];
  inviteds:any[]=[];
  users:any[]=[];
  houses:any[]=[];
  numUsers:number;
  numHouses:number;
  access_token:string;
  headers:any;
  url = sessionStorage.getItem('url_global') + '/events';
  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.http.get(this.url, {headers: this.headers}).subscribe( response => {
      // @ts-ignore
      this.eventsAll = response.events;
      // @ts-ignore
      this.inviteds = response.inviteds;
      this.numUsers = this.users.length;
      this.numHouses = this.houses.length;
      this.events = this.eventsAll;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  invitedsNumber(event){
    let inviteds = this.inviteds.filter(item => item.event_id === event.id);
    let numberInviteds = inviteds.length;
    return numberInviteds;
  }

  goShow(event){
    this.router.navigate(['/events', event.id]);
  }

  search(term:string){
    let eventArr:any[]=[];
    term = term.toLowerCase();

    for( let i = 0; i < this.eventsAll.length; i ++ ){
      let event = this.eventsAll[i];
      let name = event.name.toLowerCase();

      if( name.indexOf( term ) >= 0  ){
        eventArr.push(event);
      }
    }
    this.events = eventArr;
  }

}
