import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-show-event',
  templateUrl: './show-event.component.html',
  styleUrls: ['./show-event.component.sass']
})
export class ShowEventComponent implements OnInit {

  event:Event;
  inviteds:any[]=[];
  house:any;
  complex:any;
  access_token:string;
  headers:any;
  url = sessionStorage.getItem('url_global') + '/events';
  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService,
              private activatedRoute: ActivatedRoute) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.activatedRoute.params.subscribe( params =>{
      this.http.get(this.url +'/'+ params['id'] , {headers: this.headers}).subscribe( response => {
        this.event = {
          // @ts-ignore
          id: response.event.id,
          // @ts-ignore
          name: response.event.name,
          // @ts-ignore
          dateStart: response.event.dateStart,
          // @ts-ignore
          dateEnd: response.event.dateEnd,
          // @ts-ignore
          comments: response.event.comments,
        };
        // @ts-ignore
        this.inviteds = response.inviteds;
        // @ts-ignore
        this.house = response.house;
        // @ts-ignore
        this.complex = response.complex;
        this.loading.hide();
      })
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  get eventsItems() {
    return (this.event && this.event.name) ? this.event : null
  }

  get complexItems() {
    return (this.complex && this.complex.name) ? this.complex.name : null
  }

  get houseItems() {
    return (this.house && this.house.name) ? this.house.name : null
  }

}

export interface Event {
  id: string;
  name: string;
  dateStart: string;
  dateEnd: string;
  comments: string;
}
