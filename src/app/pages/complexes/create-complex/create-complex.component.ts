import { Component, OnInit } from '@angular/core';
import {faList} from '@fortawesome/free-solid-svg-icons';
import {faEye, faSave} from '@fortawesome/free-regular-svg-icons';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-complex',
  templateUrl: './create-complex.component.html',
  styleUrls: ['./create-complex.component.sass']
})
export class CreateComplexComponent implements OnInit {

  faList = faList;
  faEye = faEye;
  faSave = faSave;
  url = sessionStorage.getItem('url_global') + '/complexes';
  access_token:string;
  headers:any;

  constructor(private http: HttpClient,
              private loading: NgxSpinnerService,
              private  router: Router) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  gotoList(){
    this.router.navigate(['/complexes']);
  }

  submit(form){
    this.loading.show();
    const params = {
      name: form.value.name,
    }
    this.http.post(this.url, params, {headers: this.headers})
      .subscribe(response => {
        this.router.navigate(['/complexes']);
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
  }

}
