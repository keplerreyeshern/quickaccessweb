import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {faTrashAlt, faEye, faEdit, faPlusSquare} from '@fortawesome/free-regular-svg-icons';
import {faPowerOff} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-complexes',
  templateUrl: './complexes.component.html',
  styleUrls: ['./complexes.component.sass']
})
export class ComplexesComponent implements OnInit {

  faPowerOff = faPowerOff;
  faPlusSquare = faPlusSquare;
  faTrashAlt = faTrashAlt;
  faEye = faEye;
  faEdit = faEdit;
  complexes:any[]=[];
  users:any[]=[];
  houses:any[]=[];
  numUsers:number;
  numHouses:number;
  access_token:string;
  headers:any;
  url = sessionStorage.getItem('url_global') + '/complexes';
  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.http.get(this.url, {headers: this.headers}).subscribe( response => {
      // @ts-ignore
      this.complexes = response.complexes;
      // @ts-ignore
      this.users = response.users;
      // @ts-ignore
      this.houses = response.houses;
      this.numUsers = this.users.length;
      this.numHouses = this.houses.length;
      this.loading.hide();
    });
  }

  getNumbersHouses(complex){
    let houses = this.houses.filter(item => item.complex_id == complex.id);
    return houses.length;
  }

  getNumbersUsers(complex){
    let users = this.users.filter(item => item.complex_id == complex.id);
    return users.length;
  }

  active(complex){
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.http.post(this.url + '/active', complex, {headers: this.headers})
      .subscribe(response => {
        // @ts-ignore
        const index = this.complexes.findIndex(item => item.id == response.id);
        // @ts-ignore
        this.complexes[index].status = response.status;
        this.loading.hide();
      });
  }

  destroy(complex){
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.http.delete(this.url + '/' + complex.id, {headers: this.headers})
      .subscribe(response => {
        const index = this.complexes.findIndex(item => item.id == complex.id)
        this.complexes.splice(index, 1);
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
  }

  goEdit(complex){
    this.router.navigate(['/complexes/edit', complex.id]);
  }

  goShow(complex){
    this.router.navigate(['/complexes', complex.id]);
  }

}
