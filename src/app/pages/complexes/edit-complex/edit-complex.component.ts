import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {faList} from '@fortawesome/free-solid-svg-icons';
import {faEdit} from '@fortawesome/free-regular-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-edit-complex',
  templateUrl: './edit-complex.component.html',
  styleUrls: ['./edit-complex.component.sass']
})
export class EditComplexComponent implements OnInit {

  faEdit = faEdit;
  faList = faList;
  complex:any = {};
  access_token:string;
  headers:any;
  url = sessionStorage.getItem('url_global') + '/complexes/'

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private http: HttpClient,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.activatedRoute.params.subscribe( params =>{
      this.http.get(this.url + params['id'], {headers: this.headers}).subscribe(response => {
        this.complex = response;
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
    });
  }

  gotoList(){
    this.router.navigate(['/complexes'])
  }

  submit(form){
    this.loading.show();
    const params = {
      name: form.value.name,
    }
    this.http.put(this.url + this.complex.id, params, {headers: this.headers})
      .subscribe(response => {
        this.router.navigate(['/complexes']);
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
  }

}
