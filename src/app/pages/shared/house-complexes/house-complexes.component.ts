import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-house-complexes',
  templateUrl: './house-complexes.component.html',
  styleUrls: ['./house-complexes.component.sass']
})
export class HouseComplexesComponent implements OnInit {

  @Input() houses: any = [];
  @Input() complex: any = {};
  numHouses:number;

  constructor() { }

  ngOnInit(): void {
    let housesComplex = this.houses.filter(item => item.complex_id === this.complex.id);
    this.numHouses = housesComplex.length;
  }

}
