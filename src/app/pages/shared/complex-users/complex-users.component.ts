import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-complex-users',
  templateUrl: './complex-users.component.html',
  styleUrls: ['./complex-users.component.sass']
})
export class ComplexUsersComponent implements OnInit {

  @Input() complexes: any = [];
  @Input() user: any = {};
  name:string;

  constructor() { }

  ngOnInit(): void {
    let usersComplex = this.complexes.filter(item => item.id == this.user.complex_id);
    if (usersComplex.length > 0){
      console.log('si esta');
      this.name = usersComplex[0].name;
    }
  }

}
