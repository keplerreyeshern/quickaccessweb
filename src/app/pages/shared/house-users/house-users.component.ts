import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-house-users',
  templateUrl: './house-users.component.html',
  styleUrls: ['./house-users.component.sass']
})
export class HouseUsersComponent implements OnInit {

  @Input() user: any = {};
  @Input() houses: any = [];
  name:string;

  constructor() { }

  ngOnInit(): void {
    let houseUser = this.houses.filter(item => item.id == this.user.house_id);
    if (houseUser.length > 0){
      this.name = houseUser[0].name;
    }
  }

}
