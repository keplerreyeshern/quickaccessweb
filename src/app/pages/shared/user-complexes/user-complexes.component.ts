import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-complexes',
  templateUrl: './user-complexes.component.html',
  styleUrls: ['./user-complexes.component.sass']
})
export class UserComplexesComponent implements OnInit {

  @Input() complex: any = {};
  @Input() users: any = [];
  numUsers:number;

  constructor() { }

  ngOnInit(): void {
    let usersComplex = this.users.filter(item => item.complex_id === this.complex.id);
    this.numUsers = usersComplex.length;
  }

}
