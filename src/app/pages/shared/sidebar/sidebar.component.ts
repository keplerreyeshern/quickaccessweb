import { Component, OnInit } from '@angular/core';
import {faHome, faBuilding, faUsers, faList} from '@fortawesome/free-solid-svg-icons';
import {faAppStore, faAppStoreIos, faGooglePlay} from '@fortawesome/free-brands-svg-icons';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {

  faAppStore = faAppStore;
  faAppStoreIos = faAppStoreIos;
  faGooglePlay = faGooglePlay;
  faHome = faHome;
  faBuilding = faBuilding;
  faList = faList;
  faUsers = faUsers;
  access_token:string;
  url:string;
  urlImages:string;
  userProfile:string;
  logoCompany:string;
  nameCompany:string;

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
    this.url = sessionStorage.getItem('url_global');
    this.urlImages = sessionStorage.getItem('url_images');
    this.access_token = sessionStorage.getItem('access_token');
    const headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.http.get(this.url + '/profile', {headers}).subscribe( response => {
      // @ts-ignore
      this.userProfile = response.user.profile;
      // @ts-ignore
      this.logoCompany = this.urlImages + response.company.logo;
      // @ts-ignore
      this.nameCompany = response.company.name;
    });
  }

}
