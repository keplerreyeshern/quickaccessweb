import { Component, OnInit } from '@angular/core';
import {faUser} from '@fortawesome/free-regular-svg-icons';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  faUser = faUser;
  access_token:string;
  url:string;
  name:string;

  constructor(public router: Router,
              private http: HttpClient) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.url = sessionStorage.getItem('url_global');
    this.access_token = sessionStorage.getItem('access_token');
    const headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.http.get(this.url + '/profile', {headers}).subscribe( response => {
      // @ts-ignore
      this.name = response.user.name;
    });
  }
  destroy(url){
    sessionStorage.clear();
    window.location.reload();
    this.router.navigate([url]);
  }

}
