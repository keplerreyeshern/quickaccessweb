import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {faEdit} from '@fortawesome/free-regular-svg-icons';
import {faLockOpen} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  faLockOpen = faLockOpen;
  faEdit = faEdit;
  token: string;
  storage: string;
  user:any = {};
  company:any = {};
  complexes:any[] = [];
  houses:any[] = [];
  location:any = {
    street: '',
    number: '',
    suburb: '',
    state: '',
    postal_code: ''
  };
  direction:any;
  headers:any;
  house:any = {};
  complex:any = {};
  access_token:string;
  url = sessionStorage.getItem('url_global') + '/profile';

  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if (sessionStorage.getItem('change_password') === '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });

    this.http.get<any>(this.url, {headers: this.headers}).subscribe( response => {
      // console.log(response);
      this.user = response.user;
      this.company = response.company;
      this.complexes = response.complexes;
      this.houses = response.houses;
      this.complex = response.complex;
      this.house = response.house;
      if(response.location){
        this.location = response.location;
      }

      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  updateLocation(form){
    // this.loading.show();
    console.log(form.value);
    const params = {
      name: form.value.name,
      email: form.value.email,
      street: form.value.street,
      suburb: form.value.suburb,
      number: form.value.number,
      postal_code: form.value.postal_code,
      state: form.value.state
    }
    this.http.post<any>(this.url+'/update', params,{headers: this.headers}).subscribe( response => {
      // console.log(response);
      this.location = response.location;
      this.user = response.user;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('Se detecto un error comunicate con el administrador');
      }
    });
  }
}
