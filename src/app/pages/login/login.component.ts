import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  storage:string;
  url:string;
  access_token:string;
  user:any;
  company:any;

  constructor(private authService: AuthService,
              private router: Router,
              private spinnerService: NgxSpinnerService) {
    if (sessionStorage.getItem('access_token')){
      this.router.navigate(['profile']);
    }
  }

  ngOnInit(): void {
    this.url = sessionStorage.getItem('url_global');
  }

  submit(form: NgForm){
    this.spinnerService.show();
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'CbAqirTSaRHcN3UBVK7o2jHEHSrhI3tEtPOWn43f',
      username: form.value.email,
      password: form.value.password,
    };
    this.authService.postToken(params).subscribe( response => {
      // console.log(response);
      this.spinnerService.hide();
      sessionStorage.setItem('token', JSON.stringify(response));
      this.storage = sessionStorage.getItem('token');
      let start = this.storage.indexOf('access_token', 0);
      let substr = this.storage.substring(start);
      start = substr.indexOf(':', 0) + 2;
      substr = substr.substring(start);
      const end = substr.indexOf('"', 0);
      this.access_token = substr.substring(0, end );
      sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
      this.getData(sessionStorage.getItem('access_token'));
    }, err => {
      if(err.status == 400){
        this.spinnerService.hide();
        alert('Las credeciales no coinciden con la base de datos verifica y vuelve a intentar');
      } else if(err.status == 500){
        this.spinnerService.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.spinnerService.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getData(params){
    this.authService.getDataUser(this.url, params).subscribe( response => {
      this.user = response.user;
      this.company = response.company;
      sessionStorage.setItem('userProfile', response.user.profile);
      sessionStorage.setItem('userName', response.user.name);
      sessionStorage.setItem('userId', response.user.id);
      sessionStorage.setItem('change_password', response.user.password_active);

      console.log(sessionStorage.getItem('userName'));
      console.log(sessionStorage.getItem('userProfile'));
      console.log(sessionStorage.getItem('userId'));

      if(!this.company.status){
        sessionStorage.clear();
        alert('No tienes acceso ya que se bloqueo a tu empresa o residencial');
      } else {
        if (!this.user.status){
          sessionStorage.clear();
          alert('El usuario esta bloqueado y no tiene acceso a la aplicación');
        } else if (this.user.status){
          this.router.navigate(['/profile']);
        }
      }
    }, err => {
      if(err.status == 500){
        this.spinnerService.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.spinnerService.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
