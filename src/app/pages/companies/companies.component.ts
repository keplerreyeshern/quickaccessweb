import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {faTrashAlt, faEye, faEdit, faPlusSquare, faSave} from '@fortawesome/free-regular-svg-icons';
import {faPowerOff, faBan} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.sass']
})
export class CompaniesComponent implements OnInit {

  faPowerOff = faPowerOff;
  faBan = faBan;
  faSave = faSave;
  faPlusSquare = faPlusSquare;
  faTrashAlt = faTrashAlt;
  faEye = faEye;
  faEdit = faEdit;
  itemId:number;
  nameCompany:string;
  companies:any[]=[];
  users:any[]=[];
  houses:any[]=[];
  complexes:any[]=[];
  numUsers:number;
  numHouses:number;
  access_token:string;
  headers:any;
  url = sessionStorage.getItem('url_global') + '/companies';
  urlImages = sessionStorage.getItem('url_images');
  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if (sessionStorage.getItem('userProfile') != 'super_admin'){
      this.router.navigate(['/profile']);
    } else if (sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.http.get(this.url, {headers: this.headers}).subscribe( response => {
      // @ts-ignore
      this.companies = response.companies;
      // @ts-ignore
      this.users = response.users;
      // @ts-ignore
      this.houses = response.houses;
      // @ts-ignore
      this.complexes = response.complexes;
      this.numUsers = this.users.length;
      this.numHouses = this.houses.length;
      this.loading.hide();
    });
  }


  active(company){
    this.loading.show();
    this.http.get(this.url + '/' + company.id, {headers: this.headers})
      .subscribe(response => {
        // @ts-ignore
        const index = this.companies.findIndex(item => item.id == response.id);
        // @ts-ignore
        this.companies[index].status = response.status;
        this.loading.hide();
      });
  }

  destroy(company){
    const status = confirm('¿Seguro que decea eliminar la empresa ' + company.name + '?');
    if (status) {
      this.loading.show();
      this.http.delete(this.url + '/' + company.id, {headers: this.headers})
        .subscribe(response => {
          // @ts-ignore
          const index = this.companies.findIndex(item => item.id == response.id)
          this.companies.splice(index, 1);
          this.loading.hide();
        });
    }
  }

  usersNumber(company) {
    const usersnumber = this.users.filter(item => item.company_id == company.id);
    return usersnumber.length;
  }

  housesNumber(company) {
    const housesnumber = this.houses.filter(item => item.company_id == company.id);
    return housesnumber.length;
  }

  complexesNumber(company) {
    const complexesnumber = this.complexes.filter(item => item.company_id == company.id);
    return complexesnumber.length;
  }

  cancel(){
    this.nameCompany = '';
    this.itemId = 0;
  }

  edit(company){
    this.nameCompany = company.name;
    this.itemId = company.id;
  }

  update(company){
    this.loading.show();
    const params = {
      name: this.nameCompany,
    };
    this.http.put(this.url + '/' + company.id, params, {headers: this.headers})
      .subscribe(response => {
        console.log(response);
        // @ts-ignore
        const index = this.companies.findIndex(item => item.id == response.id);
        // @ts-ignore
        this.companies[index].name = response.name;
        this.loading.hide();
        this.cancel();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
  }
}
