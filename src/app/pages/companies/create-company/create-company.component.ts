import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {faList} from '@fortawesome/free-solid-svg-icons';
import {faEye, faSave} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.sass']
})
export class CreateCompanyComponent implements OnInit {

  faList = faList;
  faEye = faEye;
  faSave = faSave;
  image:any;
  thumbnailImage:any;
  url = sessionStorage.getItem('url_global') + '/companies';
  url_user = sessionStorage.getItem('url_global') + '/users';
  access_token:string;
  headers:any;

  constructor(private http: HttpClient,
              private loading: NgxSpinnerService,
              private  router: Router) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'super_admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  store(form){
  this.loading.show();
  this.http.get(this.url, {headers: this.headers})
    .subscribe(respon => {
      // @ts-ignore
      let name = respon.companies.filter(item => item.name == form.value.name);
      if (name.length > 0){
        this.loading.hide();
        alert('El nombre que ingresa ya se encuentra en la base de datos, intente con otro');
      } else {
        this.http.get(this.url_user, {headers: this.headers}).subscribe( res => {
          // @ts-ignore
          let userEmail = res.users.filter(item => item.email == form.value.email);
          // @ts-ignore
          let userPhone = res.users.filter(item => item.telephone == form.value.telephone);
          if (userEmail.length > 0) {
            this.loading.hide();
            alert('El email que ingresa ya se encuentra en la base de datos, intente con otro');
          } else if (userPhone.length > 0) {
            this.loading.hide();
            alert('El telefono que ingresa ya se encuentra en la base de datos, intente con otro');
          } else {
            const params = new FormData();
            params.append('name', form.value.name);
            params.append('contact', form.value.contact);
            params.append('email', form.value.email);
            params.append('telephone', form.value.telephone);
            params.append('logo', this.image);
            this.http.post(this.url, params, {headers: this.headers})
              .subscribe(response => {
                console.log(response);
                this.router.navigate(['/companies']);
                this.loading.hide();
              });
          }
        });
      }
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getImage(e){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file){
    let reader = new FileReader();
    reader.onload = (e) => {
      // @ts-ignore
      this.thumbnailImage = e.target.result;
    }

    reader.readAsDataURL(file);
  }
}
