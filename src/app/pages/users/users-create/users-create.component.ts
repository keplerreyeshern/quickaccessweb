import { Component, OnInit } from '@angular/core';
import {faSave, faList} from '@fortawesome/free-solid-svg-icons';
import {faEye,faEyeSlash} from '@fortawesome/free-regular-svg-icons';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.sass']
})
export class UsersCreateComponent implements OnInit {

  faSave = faSave;
  faList = faList;
  faEye = faEye;
  faEyeSlash = faEyeSlash;
  form:any;
  typeC='password';
  typeCC='password';
  headers:any;
  url = sessionStorage.getItem('url_global') + '/users';
  access_token:string;

  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  submit(form){
    if (form.value.password != form.value.pass){
      alert('Las contraseñas no coinciden, intentalo nuevamente');
    } else {
      this.loading.show();
      this.http.get(this.url, {headers: this.headers}).subscribe( response => {
        // @ts-ignore
        let userEmail = response.users.filter(item => item.email == form.value.email);
        // @ts-ignore
        let userPhone = response.users.filter(item => item.telephone == form.value.telephone);

        if (userEmail.length > 0){
          this.loading.hide();
          alert('El email que ingresa ya se encuentra en la base de datos, intente con otro');
        } else if (userPhone.length > 0){
          this.loading.hide();
          alert('El telefono que ingresa ya se encuentra en la base de datos, intente con otro');
        } else {
          let params = {
            name: form.value.name,
            email: form.value.email,
            telephone: form.value.telephone,
            password: form.value.password,
            guardhouse: form.value.guardhouse,
          };
          this.http.post(this.url, params, {headers: this.headers}).subscribe(response => {
            this.router.navigate(['/users']);
            this.loading.hide();
          }, err => {
            if(err.status == 500){
              this.loading.hide();
              alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
            } else {
              this.loading.hide();
              alert('se detecto un error comunicate con el administrador');
            }
          });
        }
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
    }
  }

  eye(){
    if(this.typeC === 'text'){
      this.typeC = 'password';
    } else {
      this.typeC = 'text';
    }
  }
  eyeC(){
    if(this.typeCC === 'text'){
      this.typeCC = 'password';
    } else {
      this.typeCC = 'text';
    }
  }

}
