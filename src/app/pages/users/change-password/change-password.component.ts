import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {AuthService} from '../../../services/auth.service';
import {faSave, faList, faSignInAlt} from '@fortawesome/free-solid-svg-icons';
import {faEye,faEyeSlash} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass']
})
export class ChangePasswordComponent implements OnInit {

  faSave = faSave;
  faList = faList;
  faEye = faEye;
  faSignInAlt = faSignInAlt;
  faEyeSlash = faEyeSlash;
  typeC='password';
  typeCC='password';
  headers:any;
  userId = sessionStorage.getItem('userId');
  url = sessionStorage.getItem('url_global') + '/users/change/password';
  access_token:string;

  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService,
              private authService: AuthService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    }
  }

  ngOnInit(): void {
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  submit(form){
    if (form.value.password != form.value.pass){
      alert('Las contraseñas deben coincidir');
    } else {
      let params = {
        password: form.value.password,
      };
      // console.log(params);
      this.http.post(this.url + '/'+ this.userId, params, {headers: this.headers} )
        .subscribe(response => {
          // @ts-ignore
          sessionStorage.setItem('change_password', response.password_active);
          this.router.navigate(['/profile']);
        }, err => {
          if(err.status == 500){
            this.loading.hide();
            alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
          } else {
            this.loading.hide();
            alert('se detecto un error comunicate con el administrador');
          }
        });
    }
  }

  eye(){
    if(this.typeC === 'text'){
      this.typeC = 'password';
    } else {
      this.typeC = 'text';
    }
  }
  eyeC(){
    if(this.typeCC === 'text'){
      this.typeCC = 'password';
    } else {
      this.typeCC = 'text';
    }
  }

}
