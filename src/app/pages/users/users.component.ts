import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {faPowerOff, faTrashAlt, faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import {faPlusSquare, faEnvelope, faEye} from '@fortawesome/free-regular-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit {

  faPowerOff = faPowerOff;
  faEye = faEye;
  faTrashAlt = faTrashAlt;
  faChevronDown = faChevronDown;
  faChevronUp = faChevronUp;
  faPlusSquare = faPlusSquare;
  faEnvelope = faEnvelope;
  selectFilter:string;
  users:any = [];
  usersAll:any = [];
  usersVigilant:any = [];
  houses:any = [];
  complexes:any = [];
  headers:any;
  orderVV = false;
  orderV = false;
  orderComplexV = false;
  orderHouseV = false;
  url = sessionStorage.getItem('url_global') + '/users';
  access_token:string;

  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });

    this.http.get(this.url, {headers: this.headers}).subscribe( response => {
      // @ts-ignore
      let usersAllT = response.users.filter(item => item.profile == 'user');
      // @ts-ignore
      this.usersVigilant = response.users.filter(item => item.profile == 'vigilant');
      // @ts-ignore
      let complexesT = response.complexes;
      // @ts-ignore
      let housesT = response.houses;
      for (let u = 0; u < usersAllT.length; u++){
        this.usersAll.push(usersAllT[u]);
        for (let c=0; c < complexesT.length; c++){
          if(usersAllT[u].complex_id == complexesT[c].id){
            this.usersAll[u].complex = complexesT[c].name;
          }
        }
        for (let h=0; h < housesT.length; h++){
          if(usersAllT[u].house_id == housesT[h].id){
            this.usersAll[u].house = housesT[h].name;
          }
        }
      }
      // @ts-ignore
      this.houses = response.houses;
      // @ts-ignore
      this.complexes = response.complexes;
      this.users = this.usersAll;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(user){
    this.loading.show();
    this.http.post(this.url + '/active', user, {headers: this.headers}).subscribe(response => {
      // @ts-ignore
      let index = this.users.findIndex(item => item.id == response.id);
      // @ts-ignore
      this.users[index].status = response.status;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  activeV(user){
    this.loading.show();
    this.http.post(this.url + '/active', user, {headers: this.headers}).subscribe(response => {
      // @ts-ignore
      let index = this.usersVigilant.findIndex(item => item.id == response.id);
      // @ts-ignore
      this.usersVigilant[index].status = response.status;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  destroy(user){
    let result = confirm('¿Estás seguro de que deseas eliminar al usuario?');
    if (result){
      this.loading.show();
      this.http.delete(this.url + '/' + user.id, {headers: this.headers}).subscribe(response => {
        let index = this.users.findIndex(item => item.id == user.id);
        this.users.splice(index,1);
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
    }
  }

  destroyV(user){
    let result = confirm('¿Estás seguro de que deseas eliminar al usuario?');
    if (result){
      this.loading.show();
      this.http.delete(this.url + '/' + user.id, {headers: this.headers}).subscribe(response => {
        let index = this.usersVigilant.findIndex(item => item.id == user.id);
        this.usersVigilant.splice(index,1);
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
    }
  }

  filter(){
    let option:number;
    if(this.selectFilter == 'all'){
      this.users = this.usersAll;
    } else {
      if(this.selectFilter == 'active'){
        option = 1;
      } else if(this.selectFilter == 'wait'){
        option = 2;
      } else if(this.selectFilter == 'canceled'){
        option = 0;
      }
      this.users = this.usersAll.filter(item => item.status == option);
    }
  }
  show(user){

  }

  orderNameV(){
    if(!this.orderVV){
      this.usersVigilant.sort((a, b) => a.name > b.name);
      this.orderVV = true;
    } else {
      this.usersVigilant.sort((a, b) => a.name < b.name);
      this.orderVV = false;
    }
  }

  orderName(){
    if(!this.orderV){
      this.users.sort((a, b) => a.name > b.name);
      this.orderV = true;
    } else {
      this.users.sort((a, b) => a.name < b.name);
      this.orderV = false;
    }
  }

  orderHouse(){
    if(!this.orderHouseV){
      this.users.sort((a, b) => a.house > b.house);
      this.orderHouseV = true;
    } else {
      this.users.sort((a, b) => a.house < b.house);
      this.orderHouseV = false;
    }
  }

  orderComplex(){
    if(!this.orderComplexV){
      this.users.sort((a, b) => a.complex > b.complex);
      this.orderComplexV = true;
    } else {
      this.users.sort((a, b) => a.complex < b.complex);
      this.orderComplexV = false;
    }
  }

}
