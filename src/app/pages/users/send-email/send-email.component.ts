import { Component, OnInit } from '@angular/core';
import {faList, faSave} from '@fortawesome/free-solid-svg-icons';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';

@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.sass']
})
export class SendEmailComponent implements OnInit {

  faList = faList;
  faSave = faSave;
  form:any;
  headers:any;
  complexes:any;
  url = sessionStorage.getItem('url_global') + '/users';
  urlComplexes = sessionStorage.getItem('url_global') + '/complexes';
  access_token:string;

  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if(sessionStorage.getItem('userProfile') != 'admin'){
      this.router.navigate(['/profile']);
    } else if(sessionStorage.getItem('change_password') == '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.http.get(this.urlComplexes, {headers: this.headers}).subscribe( response => {
      // @ts-ignore
      this.complexes = response.complexes;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }


  submit(form){
    this.http.get(this.url, {headers: this.headers}).subscribe( response => {
      // @ts-ignore
      let userEmail = response.users.filter(item => item.email == form.value.email);
      // @ts-ignore
      let userPhone = response.users.filter(item => item.telephone == form.value.telephone);
      if (userEmail.length > 0){
        alert('El email que ingresa ya se encuentra en la base de datos');
      } else if (userPhone.length > 0){
        alert('El telefono que ingresa ya se encuentra en la base de datos');
      } else {
        this.loading.show();
        let params = {
          name: form.value.name,
          email: form.value.email,
          telephone: form.value.telephone,
          house: form.value.lastname,
          complex: form.value.complex,
          street: form.value.street,
          number: form.value.number,
        };
        this.http.post(this.url + '/save', params, {headers: this.headers}).subscribe(response => {
          this.router.navigate(['/users']);
          this.loading.hide();
        }, err => {
          if(err.status == 500){
            this.loading.hide();
            alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
          } else {
            this.loading.hide();
            alert('se detecto un error comunicate con el administrador');
          }
        });
      }
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
