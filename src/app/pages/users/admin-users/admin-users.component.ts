import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {faTrashAlt, faEye, faEdit, faPlusSquare} from '@fortawesome/free-regular-svg-icons';
import {faPowerOff} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.sass']
})
export class AdminUsersComponent implements OnInit {

  faPowerOff = faPowerOff;
  faPlusSquare = faPlusSquare;
  faTrashAlt = faTrashAlt;
  faEye = faEye;
  faEdit = faEdit;
  complexes:any[]=[];
  companies:any[]=[];
  users:any[]=[];
  usersAll:any[]=[];
  houses:any[]=[];
  numUsers:number;
  numHouses:number;
  access_token:string;
  headers:any;
  url = sessionStorage.getItem('url_global') + '/users';
  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService) {
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['login']);
    } else if (sessionStorage.getItem('userProfile') !== 'super_admin'){
      this.router.navigate(['/profile']);
    } else if (sessionStorage.getItem('change_password') === '0'){
      this.router.navigate(['/users/change/password']);
    }
  }

  ngOnInit(): void {
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
    this.http.get(this.url, {headers: this.headers}).subscribe( response => {
      // @ts-ignore
      this.complexes = response.complexes;

      // @ts-ignore
      this.companies = response.companies;

      // @ts-ignore
      this.users = response.usersAdmin;

      // @ts-ignore
      this.houses = response.houses;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(user){
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.http.post(this.url + '/active', user, {headers: this.headers})
      .subscribe(response => {
        // @ts-ignore
        const index = this.users.findIndex(item => item.id == response.id);
        // @ts-ignore
        this.users[index].status = response.status;
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
  }

  destroy(user){
    this.loading.show();
    this.access_token = sessionStorage.getItem('access_token');

    this.http.delete(this.url + '/' + user.id, {headers: this.headers})
      .subscribe(response => {
        // @ts-ignore
        const index = this.users.findIndex(item => item.id == response.id)
        this.users.splice(index, 1);
        this.loading.hide();
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
  }

  nameCompany(user): string{
    let name = '';
    for(let c = 0; c < this.companies.length; c++){
      const company_id = parseInt(this.companies[c].id, 10);
      const user_id = parseInt(user.company_id, 10);
      if (user_id === company_id){
        name = this.companies[c].name;
      }
    }
    return name;
  }

  goEdit(user): void{
    this.router.navigate(['/users/edit', user.id]);
  }

  goShow(user): void{
    this.router.navigate(['/users', user.id]);
  }

}
