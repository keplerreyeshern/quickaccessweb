import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {faSave, faList, faSignInAlt} from '@fortawesome/free-solid-svg-icons';
import {faEye,faEyeSlash} from '@fortawesome/free-regular-svg-icons';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  faSave = faSave;
  faList = faList;
  faEye = faEye;
  faSignInAlt = faSignInAlt;
  faEyeSlash = faEyeSlash;
  form:any;
  typeC='password';
  typeCC='password';
  company:any;
  user:any;
  headers:any;
  storage:any;
  url = sessionStorage.getItem('url_global');
  access_token:string;

  constructor(private http: HttpClient,
              private router: Router,
              private loading: NgxSpinnerService,
              private authService: AuthService) {
    if (sessionStorage.getItem('access_token')){
      this.router.navigate(['profile']);
    }
  }

  ngOnInit(): void {
  }

  submit(form: NgForm){
    this.loading.show();
    if (form.value.password != form.value.pass){
      this.loading.hide();
      alert('Las contraseñas no coinciden, intentalo nuevamente');
    } else {
      // this.loading.show();
      this.http.post<any>(this.url + '/users/confirm', form.value).subscribe( response => {
        let userEmail = response.filter(item => item.email == form.value.email);
        let userPhone = response.filter(item => item.telephone == form.value.telephone);
        if (userEmail.length > 0){
          this.loading.hide();
          alert('El email que ingresa ya se encuentra en la base de datos, intente con otro');
        } else if (userPhone.length > 0){
          this.loading.hide();
          alert('El telefono que ingresa ya se encuentra en la base de datos, intente con otro');
        } else {
          let params = {
            name: form.value.name,
            email: form.value.email,
            telephone: form.value.telephone,
            password: form.value.password,
            code: form.value.code,
          };
          this.http.post(this.url + '/users/register', params).subscribe(response => {
            // @ts-ignore
            if (response.status){
              let credentials = {
                email: form.value.email,
                password: form.value.password,
              };
              this.login(credentials);
            } else {
              alert('El codigo que ingreso llego a su maximo de usuarios registrados');
              this.loading.hide();
            }
          }, err => {
            if(err.status == 400){
              this.loading.hide();
              alert('Las credeciales no coinciden con la base de datos verifica y vuelve a intentar');
            } else if(err.status == 500){
              this.loading.hide();
              alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
            } else {
              this.loading.hide();
              alert('se detecto un error comunicate con el administrador');
            }
          });
        }
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
    }
  }

  eye(){
    if(this.typeC === 'text'){
      this.typeC = 'password';
    } else {
      this.typeC = 'text';
    }
  }
  eyeC(){
    if(this.typeCC === 'text'){
      this.typeCC = 'password';
    } else {
      this.typeCC = 'text';
    }
  }

  login(credentials){
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'tu4hqvNs1fN7EfHDKURPXuOe6CEXSENRRAcZwGmD',
      username: credentials.email,
      password: credentials.password,
    };
    this.authService.postToken(params).subscribe( response => {
      // console.log(response);
      if (response.error){
        this.loading.hide();
        alert(response.message);
      } else if(response.token_type){
        sessionStorage.setItem("token", JSON.stringify(response));
        this.storage = sessionStorage.getItem('token');
        let start = this.storage.indexOf('access_token', 0);
        let substr = this.storage.substring(start);
        start = substr.indexOf(':', 0) + 2;
        substr = substr.substring(start);
        const end = substr.indexOf('"', 0);
        this.access_token = substr.substring(0, end );
        sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
        this.getData(sessionStorage.getItem('access_token'));
      }
    }, err => {
      if(err.status == 400){
        this.loading.hide();
        alert('Las credeciales no coinciden con la base de datos verifica y vuelve a intentar');
      } else if(err.status == 401){
        this.loading.hide();
        alert('No se pudo iniciar sesion, intenta ingresar directamente al login');
      } else if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getData(params){
    this.authService.getDataUser(this.url, params).subscribe( response => {
      // @ts-ignore
      this.user = response.user;
      // @ts-ignore
      this.company = response.company;
      // @ts-ignore
      sessionStorage.setItem('userProfile', response.user.profile);
      // @ts-ignore
      sessionStorage.setItem('userName', response.user.name);
      // @ts-ignore
      console.log(sessionStorage.getItem('userName'));
      // @ts-ignore
      console.log(sessionStorage.getItem('userProfile'));

      if(!this.company.status){
        sessionStorage.clear();
        alert('No tienes acceso ya que se bloqueo a tu empresa o residencial');
        this.loading.hide();
      } else {
        if(!this.user.status){
          sessionStorage.clear();
          alert('El usuario esta bloqueado y no tiene acceso a la aplicación');
          this.loading.hide();
        } else if(this.user.status){
          this.router.navigate(['/profile']);
          this.loading.hide();
        }
      }
    });
  }

}
