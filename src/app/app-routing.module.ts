import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileComponent} from './pages/profile/profile.component';
import {LoginComponent} from './pages/login/login.component';
import {ComplexesComponent} from './pages/complexes/complexes.component';
import {CreateComplexComponent} from './pages/complexes/create-complex/create-complex.component';
import {EditComplexComponent} from './pages/complexes/edit-complex/edit-complex.component';
import {UsersComponent} from './pages/users/users.component';
import {UsersCreateComponent} from './pages/users/users-create/users-create.component';
import {SendEmailComponent} from './pages/users/send-email/send-email.component';
import {MainComponent} from './pages/main/main.component';
import {RegisterComponent} from './pages/users/register/register.component';
import {Error404Component} from './pages/shared/error404/error404.component';
import {EventsComponent} from './pages/events/events.component';
import {ShowEventComponent} from './pages/events/show-event/show-event.component';
import {CompaniesComponent} from './pages/companies/companies.component';
import {CreateCompanyComponent} from './pages/companies/create-company/create-company.component';
import {AdminUsersComponent} from './pages/users/admin-users/admin-users.component';
import {ChangePasswordComponent} from './pages/users/change-password/change-password.component';

const routes: Routes = [
  { path: 'companies', component: CompaniesComponent },
  { path: 'companies/create', component: CreateCompanyComponent },
  { path: 'users/admin', component: AdminUsersComponent },
  { path: 'users/change/password', component: ChangePasswordComponent },
  { path: 'users', component: UsersComponent },
  { path: 'events', component: EventsComponent },
  { path: 'events/:id', component: ShowEventComponent },
  { path: 'users/create', component: UsersCreateComponent },
  { path: 'users/send/email', component: SendEmailComponent },
  { path: 'complexes', component: ComplexesComponent },
  { path: 'complexes/create', component: CreateComplexComponent },
  { path: 'complexes/edit/:id', component: EditComplexComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '', component: MainComponent },
  { path: 'error-404', component: Error404Component },
  { path: '**', pathMatch: 'full', redirectTo: 'error-404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
