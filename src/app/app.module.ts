import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NavbarComponent} from './pages/shared/navbar/navbar.component';
import {SidebarComponent} from './pages/shared/sidebar/sidebar.component';
import {FooterComponent} from './pages/shared/footer/footer.component';
import {ComplexesComponent} from './pages/complexes/complexes.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './pages/login/login.component';
import {FormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BaseComponent } from './pages/base/base.component';
import { CreateComplexComponent } from './pages/complexes/create-complex/create-complex.component';
import { EditComplexComponent } from './pages/complexes/edit-complex/edit-complex.component';
import { UserComplexesComponent } from './pages/shared/user-complexes/user-complexes.component';
import { HouseComplexesComponent } from './pages/shared/house-complexes/house-complexes.component';
import { UsersComponent } from './pages/users/users.component';
import { HouseUsersComponent } from './pages/shared/house-users/house-users.component';
import { ComplexUsersComponent } from './pages/shared/complex-users/complex-users.component';
import { UsersCreateComponent } from './pages/users/users-create/users-create.component';
import { SendEmailComponent } from './pages/users/send-email/send-email.component';
import { MainComponent } from './pages/main/main.component';
import { RegisterComponent } from './pages/users/register/register.component';
import { Error404Component } from './pages/shared/error404/error404.component';
import { FeaturesMainComponent } from './pages/shared/features-main/features-main.component';
import { SolutionsMainComponent } from './pages/shared/solutions-main/solutions-main.component';
import { ContactMainComponent } from './pages/shared/contact-main/contact-main.component';
import { EventsComponent } from './pages/events/events.component';
import { InvitedsEventComponent } from './pages/shared/inviteds-event/inviteds-event.component';
import { ShowEventComponent } from './pages/events/show-event/show-event.component';
import { CompaniesComponent } from './pages/companies/companies.component';
import { CreateCompanyComponent } from './pages/companies/create-company/create-company.component';
import { AdminUsersComponent } from './pages/users/admin-users/admin-users.component';
import { ChangePasswordComponent } from './pages/users/change-password/change-password.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    ComplexesComponent,
    ProfileComponent,
    LoginComponent,
    BaseComponent,
    CreateComplexComponent,
    EditComplexComponent,
    UserComplexesComponent,
    HouseComplexesComponent,
    UsersComponent,
    HouseUsersComponent,
    ComplexUsersComponent,
    UsersCreateComponent,
    SendEmailComponent,
    MainComponent,
    RegisterComponent,
    Error404Component,
    FeaturesMainComponent,
    SolutionsMainComponent,
    ContactMainComponent,
    EventsComponent,
    InvitedsEventComponent,
    ShowEventComponent,
    CompaniesComponent,
    CreateCompanyComponent,
    AdminUsersComponent,
    ChangePasswordComponent,
  ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      FontAwesomeModule,
      HttpClientModule,
      FormsModule,
      BrowserAnimationsModule,
      NgxSpinnerModule,
    ],
  providers: [],
  exports: [
    NavbarComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
