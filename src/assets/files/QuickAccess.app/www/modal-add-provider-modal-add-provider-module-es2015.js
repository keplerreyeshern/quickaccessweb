(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modal-add-provider-modal-add-provider-module"],{

/***/ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider-routing.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: ModalAddProviderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalAddProviderPageRoutingModule", function() { return ModalAddProviderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _modal_add_provider_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal-add-provider.page */ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts");




const routes = [
    {
        path: '',
        component: _modal_add_provider_page__WEBPACK_IMPORTED_MODULE_3__["ModalAddProviderPage"]
    }
];
let ModalAddProviderPageRoutingModule = class ModalAddProviderPageRoutingModule {
};
ModalAddProviderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ModalAddProviderPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.module.ts ***!
  \******************************************************************************************/
/*! exports provided: ModalAddProviderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalAddProviderPageModule", function() { return ModalAddProviderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _modal_add_provider_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modal-add-provider-routing.module */ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider-routing.module.ts");
/* harmony import */ var _modal_add_provider_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal-add-provider.page */ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../components/components.module */ "./src/app/components/components.module.ts");








let ModalAddProviderPageModule = class ModalAddProviderPageModule {
};
ModalAddProviderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _modal_add_provider_routing_module__WEBPACK_IMPORTED_MODULE_5__["ModalAddProviderPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_modal_add_provider_page__WEBPACK_IMPORTED_MODULE_6__["ModalAddProviderPage"]]
    })
], ModalAddProviderPageModule);



/***/ })

}]);
//# sourceMappingURL=modal-add-provider-modal-add-provider-module-es2015.js.map