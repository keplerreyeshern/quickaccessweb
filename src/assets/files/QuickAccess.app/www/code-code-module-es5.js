(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["code-code-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/code/code.page.html":
    /*!**************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/code/code.page.html ***!
      \**************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserCodeCodePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header-initial title=\"Codigo de Acceso\"></app-header-initial>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/user/code/code-routing.module.ts":
    /*!********************************************************!*\
      !*** ./src/app/pages/user/code/code-routing.module.ts ***!
      \********************************************************/

    /*! exports provided: CodePageRoutingModule */

    /***/
    function srcAppPagesUserCodeCodeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CodePageRoutingModule", function () {
        return CodePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _code_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./code.page */
      "./src/app/pages/user/code/code.page.ts");

      var routes = [{
        path: '',
        component: _code_page__WEBPACK_IMPORTED_MODULE_3__["CodePage"]
      }];

      var CodePageRoutingModule = function CodePageRoutingModule() {
        _classCallCheck(this, CodePageRoutingModule);
      };

      CodePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CodePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/user/code/code.module.ts":
    /*!************************************************!*\
      !*** ./src/app/pages/user/code/code.module.ts ***!
      \************************************************/

    /*! exports provided: CodePageModule */

    /***/
    function srcAppPagesUserCodeCodeModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CodePageModule", function () {
        return CodePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _code_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./code-routing.module */
      "./src/app/pages/user/code/code-routing.module.ts");
      /* harmony import */


      var _code_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./code.page */
      "./src/app/pages/user/code/code.page.ts");
      /* harmony import */


      var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../components/components.module */
      "./src/app/components/components.module.ts");

      var CodePageModule = function CodePageModule() {
        _classCallCheck(this, CodePageModule);
      };

      CodePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _code_routing_module__WEBPACK_IMPORTED_MODULE_5__["CodePageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_code_page__WEBPACK_IMPORTED_MODULE_6__["CodePage"]]
      })], CodePageModule);
      /***/
    },

    /***/
    "./src/app/pages/user/code/code.page.scss":
    /*!************************************************!*\
      !*** ./src/app/pages/user/code/code.page.scss ***!
      \************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserCodeCodePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  height: 100%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9jb2RlL2NvZGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FBRUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91c2VyL2NvZGUvY29kZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzFmMWYxZjtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/user/code/code.page.ts":
    /*!**********************************************!*\
      !*** ./src/app/pages/user/code/code.page.ts ***!
      \**********************************************/

    /*! exports provided: CodePage */

    /***/
    function srcAppPagesUserCodeCodePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CodePage", function () {
        return CodePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CodePage = /*#__PURE__*/function () {
        function CodePage() {
          _classCallCheck(this, CodePage);
        }

        _createClass(CodePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CodePage;
      }();

      CodePage.ctorParameters = function () {
        return [];
      };

      CodePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-code',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./code.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/code/code.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./code.page.scss */
        "./src/app/pages/user/code/code.page.scss"))["default"]]
      })], CodePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=code-code-module-es5.js.map