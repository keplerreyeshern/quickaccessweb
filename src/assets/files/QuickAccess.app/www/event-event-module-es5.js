(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["event-event-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/event.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/event.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserMenuEventEventPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header-secondary title=\"Crear Evento\"></app-header-secondary>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <ion-card>\n        <ion-card-header class=\"ion-margin-top ion-margin-bottom\">\n          <ion-card-subtitle class=\"ion-text-center\">¿Como se llama el Evento?</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-item class=\"ion-margin-top ion-margin-bottom\">\n            <ion-label position=\"floating\">Nombre del Evento</ion-label>\n            <ion-input [(ngModel)]=\"name\">\n            </ion-input>\n          </ion-item>\n          <ion-row class=\"ion-margin-top ion-margin-bottom\">\n            <ion-col offset=\"2\" size=\"8\">\n              <ion-button expand=\"block\" color=\"success\" (click)=\"goTo()\">\n                Siguiente\n                <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-card-content>\n      </ion-card>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/event-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/user/menu/event/event-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: EventPageRoutingModule */

    /***/
    function srcAppPagesUserMenuEventEventRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EventPageRoutingModule", function () {
        return EventPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _event_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./event.page */
      "./src/app/pages/user/menu/event/event.page.ts");

      var routes = [{
        path: '',
        component: _event_page__WEBPACK_IMPORTED_MODULE_3__["EventPage"]
      }, {
        path: 'date-start/:name',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | date-start-date-start-module */
          "date-start-date-start-module").then(__webpack_require__.bind(null,
          /*! ./date-start/date-start.module */
          "./src/app/pages/user/menu/event/date-start/date-start.module.ts")).then(function (m) {
            return m.DateStartPageModule;
          });
        }
      }, {
        path: 'date-end/:name/:dateStart',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | date-end-date-end-module */
          "date-end-date-end-module").then(__webpack_require__.bind(null,
          /*! ./date-end/date-end.module */
          "./src/app/pages/user/menu/event/date-end/date-end.module.ts")).then(function (m) {
            return m.DateEndPageModule;
          });
        }
      }, {
        path: 'contacts/:name/:dateStart/:dateEnd',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | contacts-contacts-module */
          "contacts-contacts-module").then(__webpack_require__.bind(null,
          /*! ./contacts/contacts.module */
          "./src/app/pages/user/menu/event/contacts/contacts.module.ts")).then(function (m) {
            return m.ContactsPageModule;
          });
        }
      }, {
        path: 'comments/:name/:dateStart/:dateEnd',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | comments-comments-module */
          "comments-comments-module").then(__webpack_require__.bind(null,
          /*! ./comments/comments.module */
          "./src/app/pages/user/menu/event/comments/comments.module.ts")).then(function (m) {
            return m.CommentsPageModule;
          });
        }
      }, {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
      }];

      var EventPageRoutingModule = function EventPageRoutingModule() {
        _classCallCheck(this, EventPageRoutingModule);
      };

      EventPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EventPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/event.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/user/menu/event/event.module.ts ***!
      \*******************************************************/

    /*! exports provided: EventPageModule */

    /***/
    function srcAppPagesUserMenuEventEventModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EventPageModule", function () {
        return EventPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _event_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./event-routing.module */
      "./src/app/pages/user/menu/event/event-routing.module.ts");
      /* harmony import */


      var _event_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./event.page */
      "./src/app/pages/user/menu/event/event.page.ts");
      /* harmony import */


      var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../components/components.module */
      "./src/app/components/components.module.ts");

      var EventPageModule = function EventPageModule() {
        _classCallCheck(this, EventPageModule);
      };

      EventPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _event_routing_module__WEBPACK_IMPORTED_MODULE_5__["EventPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_event_page__WEBPACK_IMPORTED_MODULE_6__["EventPage"]]
      })], EventPageModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/event.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/pages/user/menu/event/event.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserMenuEventEventPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-slides, ion-slide {\n  height: 100%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50L2V2ZW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSwrQkFBQTtBQUdGOztBQURBO0VBQ0UsK0JBQUE7QUFJRjs7QUFGQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FBS0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91c2VyL21lbnUvZXZlbnQvZXZlbnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzEwMTAxMDtcbn1cbmlvbi10aXRsZXtcbiAgY29sb3I6ICNkN2Q4ZGE7XG59XG5pb24tY29udGVudHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzFmMWYxZjtcbn1cbmlvbi1jYXJke1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZjNmMGYwO1xufVxuaW9uLXNsaWRlcywgaW9uLXNsaWRle1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/event.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/user/menu/event/event.page.ts ***!
      \*****************************************************/

    /*! exports provided: EventPage */

    /***/
    function srcAppPagesUserMenuEventEventPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EventPage", function () {
        return EventPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var EventPage = /*#__PURE__*/function () {
        function EventPage(router, alertController) {
          _classCallCheck(this, EventPage);

          this.router = router;
          this.alertController = alertController;
        }

        _createClass(EventPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "goTo",
          value: function goTo() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        header: '!Error¡',
                        message: 'Debes ingresar el nombre del Evento',
                        buttons: ['ok']
                      });

                    case 2:
                      alert = _context.sent;

                      if (!(!this.name || this.name.length === 0)) {
                        _context.next = 8;
                        break;
                      }

                      _context.next = 6;
                      return alert.present();

                    case 6:
                      _context.next = 11;
                      break;

                    case 8:
                      this.name = this.name.replace(/ /g, '-');
                      this.router.navigateByUrl('/user/menu/event/date-start/' + this.name);
                      console.log(this.name);

                    case 11:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return EventPage;
      }();

      EventPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }];
      };

      EventPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-event',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./event.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/event.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./event.page.scss */
        "./src/app/pages/user/menu/event/event.page.scss"))["default"]]
      })], EventPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=event-event-module-es5.js.map