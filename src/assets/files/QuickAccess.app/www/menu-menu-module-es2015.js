(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["menu-menu-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/menu.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/menu.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-initial title=\"Inicio\"></app-header-initial>\n\n<ion-content>\n  <ion-list class=\"ion-margin-top\">\n    <ion-item routerLink=\"event\" color=\"dark\" detail>\n      <ion-icon slot=\"start\" name=\"ellipsis-vertical-outline\" color=\"tertiary\"></ion-icon>\n      <ion-label class=\"ion-text-capitalize\">crear evento</ion-label>\n    </ion-item>\n  </ion-list>\n  <ion-list class=\"ion-margin-top\">\n    <ion-item routerLink=\"events\" color=\"dark\" detail>\n      <ion-icon slot=\"start\" name=\"ellipsis-vertical-outline\" color=\"success\"></ion-icon>\n      <ion-label color=\"light\" class=\"ion-text-capitalize\">lista de eventos</ion-label>\n    </ion-item>\n  </ion-list>\n  <ion-list class=\"ion-margin-top\">\n    <ion-item routerLink=\"provider\" color=\"dark\" detail>\n      <ion-icon slot=\"start\" name=\"ellipsis-vertical-outline\" color=\"danger\"></ion-icon>\n      <ion-label class=\"ion-text-capitalize\">proveedores</ion-label>\n    </ion-item>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/user/menu/menu-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/user/menu/menu-routing.module.ts ***!
  \********************************************************/
/*! exports provided: MenuPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageRoutingModule", function() { return MenuPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu.page */ "./src/app/pages/user/menu/menu.page.ts");




const routes = [
    {
        path: '',
        component: _menu_page__WEBPACK_IMPORTED_MODULE_3__["MenuPage"]
    },
    {
        path: 'event',
        loadChildren: () => __webpack_require__.e(/*! import() | event-event-module */ "event-event-module").then(__webpack_require__.bind(null, /*! ./event/event.module */ "./src/app/pages/user/menu/event/event.module.ts")).then(m => m.EventPageModule)
    },
    {
        path: 'events',
        loadChildren: () => Promise.all(/*! import() | events-events-module */[__webpack_require__.e("common"), __webpack_require__.e("events-events-module")]).then(__webpack_require__.bind(null, /*! ./events/events.module */ "./src/app/pages/user/menu/events/events.module.ts")).then(m => m.EventsPageModule)
    },
    {
        path: 'provider',
        loadChildren: () => Promise.all(/*! import() | provider-provider-module */[__webpack_require__.e("common"), __webpack_require__.e("provider-provider-module")]).then(__webpack_require__.bind(null, /*! ./provider/provider.module */ "./src/app/pages/user/menu/provider/provider.module.ts")).then(m => m.ProviderPageModule)
    }
];
let MenuPageRoutingModule = class MenuPageRoutingModule {
};
MenuPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MenuPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/menu/menu.module.ts":
/*!************************************************!*\
  !*** ./src/app/pages/user/menu/menu.module.ts ***!
  \************************************************/
/*! exports provided: MenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageModule", function() { return MenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu-routing.module */ "./src/app/pages/user/menu/menu-routing.module.ts");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu.page */ "./src/app/pages/user/menu/menu.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let MenuPageModule = class MenuPageModule {
};
MenuPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["MenuPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"]]
    })
], MenuPageModule);



/***/ }),

/***/ "./src/app/pages/user/menu/menu.page.scss":
/*!************************************************!*\
  !*** ./src/app/pages/user/menu/menu.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n  margin: auto 0;\n}\n\nion-label {\n  color: #d7d8da;\n}\n\nion-item {\n  padding: 1rem 0;\n}\n\nion-slides, ion-slide {\n  height: 100%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L21lbnUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLCtCQUFBO0VBQ0EsY0FBQTtBQUdGOztBQURBO0VBQ0UsY0FBQTtBQUlGOztBQUZBO0VBQ0UsZUFBQTtBQUtGOztBQUhBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7QUFNRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvbWVudS9tZW51LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxMDEwMTA7XG59XG5pb24tdGl0bGV7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNvbnRlbnR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxZjFmMWY7XG4gIG1hcmdpbjogYXV0byAwO1xufVxuaW9uLWxhYmVse1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1pdGVte1xuICBwYWRkaW5nOiAxcmVtIDA7XG59XG5pb24tc2xpZGVzLCBpb24tc2xpZGV7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/user/menu/menu.page.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/user/menu/menu.page.ts ***!
  \**********************************************/
/*! exports provided: MenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPage", function() { return MenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let MenuPage = class MenuPage {
    constructor() { }
    ngOnInit() {
    }
};
MenuPage.ctorParameters = () => [];
MenuPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./menu.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/menu.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./menu.page.scss */ "./src/app/pages/user/menu/menu.page.scss")).default]
    })
], MenuPage);



/***/ })

}]);
//# sourceMappingURL=menu-menu-module-es2015.js.map