(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contacts-contacts-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/contacts.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/contacts.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-secondary title=\"contactos\"></app-header-secondary>\n\n<ion-content>\n    <ion-searchbar animated\n                   (ionChange)=\"search($event)\"\n                   placeholder=\"Buscar contacto\">\n    </ion-searchbar>\n    <ion-list>\n        <ion-item *ngFor=\"let contact of listContacts | filter:contactSearch\" (click)=\"select(contact)\" color=\"dark\">\n            <ion-avatar item-start>\n                <img [src]=\"contact.avatar[0].value\">\n            </ion-avatar>\n            <ion-label class=\"ion-margin-start\">{{contact.displayName }}</ion-label>\n            <ion-checkbox slot=\"end\" color=\"primary\"></ion-checkbox>\n        </ion-item>\n        <ion-item color=\"dark\">\n            <ion-avatar item-start>\n            </ion-avatar>\n            <ion-label></ion-label>\n        </ion-item>\n    </ion-list>\n    <!-- fab placed to the bottom end -->\n    <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\">\n        <ion-fab-button (click)=\"openModal()\">\n            <ion-icon name=\"add\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n    <!-- fab placed to the bottom end -->\n    <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button (click)=\"goTo()\">\n            <ion-icon name=\"arrow-forward-outline\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/modal/modal.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/modal/modal.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Agregar Nuevo Invitado</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <form #form=\"ngForm\" (ngSubmit)=\"save(form)\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\">Nombre Completo</ion-label>\n            <ion-input ngModel\n                       type=\"text\"\n                       name=\"name\"\n                       required>\n            </ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Email</ion-label>\n            <ion-input type=\"email\"\n                       name=\"email\"\n                       pattern=\"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$\"\n                       ngModel>\n            </ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Numero de Celular</ion-label>\n            <ion-input type=\"number\"\n                       name=\"phone\"\n                       ngModel\n                       minlength=\"10\"\n                       maxlength=\"10\"\n                       required>\n            </ion-input>\n          </ion-item>\n          <ion-row>\n            <ion-col offset=\"1\" size=\"10\">\n              <ion-button type=\"submit\" color=\"success\" size=\"large\" [disabled]=\"form.invalid\" expand=\"block\">\n                <ion-icon name=\"save-outline\" slot=\"start\"></ion-icon>\n                Guardar\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-list>\n      </form>\n    </ion-slide>\n  </ion-slides>\n  <!-- fab placed to the bottom end -->\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"danger\" fill=\"outline\" (click)=\"exit()\">\n      <ion-icon name=\"close-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/user/menu/event/contacts/contacts-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/user/menu/event/contacts/contacts-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: ContactsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPageRoutingModule", function() { return ContactsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _contacts_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contacts.page */ "./src/app/pages/user/menu/event/contacts/contacts.page.ts");




const routes = [
    {
        path: '',
        component: _contacts_page__WEBPACK_IMPORTED_MODULE_3__["ContactsPage"]
    },
    {
        path: 'modal',
        loadChildren: () => __webpack_require__.e(/*! import() | modal-modal-module */ "modal-modal-module").then(__webpack_require__.bind(null, /*! ./modal/modal.module */ "./src/app/pages/user/menu/event/contacts/modal/modal.module.ts")).then(m => m.ModalPageModule)
    }
];
let ContactsPageRoutingModule = class ContactsPageRoutingModule {
};
ContactsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/menu/event/contacts/contacts.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/user/menu/event/contacts/contacts.module.ts ***!
  \*******************************************************************/
/*! exports provided: ContactsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPageModule", function() { return ContactsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacts-routing.module */ "./src/app/pages/user/menu/event/contacts/contacts-routing.module.ts");
/* harmony import */ var _contacts_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contacts.page */ "./src/app/pages/user/menu/event/contacts/contacts.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ContactsPageModule = class ContactsPageModule {
};
ContactsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactsPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]
        ],
        declarations: [_contacts_page__WEBPACK_IMPORTED_MODULE_6__["ContactsPage"]]
    })
], ContactsPageModule);



/***/ }),

/***/ "./src/app/pages/user/menu/event/contacts/contacts.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/user/menu/event/contacts/contacts.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-searchbar {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-slides, ion-slide {\n  height: 100%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50L2NvbnRhY3RzL2NvbnRhY3RzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSxjQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtBQUlGOztBQUZBO0VBQ0UsK0JBQUE7QUFLRjs7QUFIQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FBTUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91c2VyL21lbnUvZXZlbnQvY29udGFjdHMvY29udGFjdHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzEwMTAxMDtcbn1cbmlvbi10aXRsZXtcbiAgY29sb3I6ICNkN2Q4ZGE7XG59XG5pb24tc2VhcmNoYmFye1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jb250ZW50e1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMWYxZjFmO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tc2xpZGVzLCBpb24tc2xpZGV7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/user/menu/event/contacts/contacts.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/user/menu/event/contacts/contacts.page.ts ***!
  \*****************************************************************/
/*! exports provided: ContactsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPage", function() { return ContactsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _modal_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal/modal.page */ "./src/app/pages/user/menu/event/contacts/modal/modal.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_create_invited_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/create-invited.service */ "./src/app/services/create-invited.service.ts");
/* harmony import */ var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/contacts/ngx */ "./node_modules/@ionic-native/contacts/__ivy_ngcc__/ngx/index.js");







let ContactsPage = class ContactsPage {
    constructor(modalCtrl, router, contacts, activatedRoute, loading, alertController, service) {
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.contacts = contacts;
        this.activatedRoute = activatedRoute;
        this.loading = loading;
        this.alertController = alertController;
        this.service = service;
        this.listContacts = [];
        this.avatar = './assets/icon/avatar.png';
        this.status = false;
        this.inviteds = [];
        this.contactSearch = '';
    }
    ngOnInit() {
        this.loadingContacts();
        this.activatedRoute.params.subscribe(params => {
            this.name = params.name;
            this.dateStart = params.dateStart;
            this.dateEnd = params.dateEnd.substr(0, 19);
        });
    }
    loadingContacts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading3 = yield this.loading.create({
                message: 'Cargando...'
            });
            yield loading3.present();
            this.contacts.find(['*'])
                .then(res => {
                const readData = [];
                res.map((item) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    readData.push({
                        displayName: item.displayName,
                        photos: item.photos,
                        avatar: [{ value: this.avatar }],
                        phoneNumbers: item.phoneNumbers,
                        emails: item.emails,
                    });
                }));
                this.listContacts = readData;
                // tslint:disable-next-line:only-arrow-functions
                this.listContacts.sort(function (a, b) {
                    if (a.displayName > b.displayName) {
                        return 1;
                    }
                    if (a.displayName < b.displayName) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }, (error) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                console.log(error);
                yield loading3.dismiss();
            }));
            yield loading3.dismiss();
        });
    }
    search(event) {
        this.contactSearch = event.detail.value;
    }
    select(contact) {
        let email;
        console.log(contact.emails);
        // @ts-ignore
        if (contact.emails == null) {
            email = null;
        }
        else {
            email = contact.emails[0].value;
        }
        const invited = {
            name: contact.displayName,
            email,
            phone: contact.phoneNumbers[0].value
        };
        let index;
        let verified = false;
        if (this.inviteds.length > 0) {
            for (let i = 0; i < this.inviteds.length; i++) {
                if (this.inviteds[i].name === contact.displayName) {
                    verified = true;
                    index = i;
                }
            }
        }
        if (verified) {
            this.inviteds.splice(index, 1);
        }
        else {
            this.inviteds.push(invited);
        }
        console.log(this.inviteds);
    }
    goTo() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(this.inviteds.length);
            const alert = yield this.alertController.create({
                header: '!Error¡',
                message: 'Debes agregar o seleccionar al menos un invitado',
                buttons: ['ok']
            });
            if (!this.status && this.inviteds.length === 0) {
                yield alert.present();
            }
            else {
                const load = yield this.alertController.create({
                    message: 'Guardando...'
                });
                yield load.present();
                // tslint:disable-next-line:prefer-for-of
                for (let j = 0; j < this.inviteds.length; j++) {
                    this.save(this.inviteds[j]);
                }
                const alert2 = yield this.alertController.create({
                    header: '!Exito¡',
                    message: 'Invitados guardados con exito',
                    buttons: ['ok']
                });
                yield load.dismiss();
                yield alert2.present();
                this.router.navigateByUrl('/tabs/home/event-create/comments/' + this.name + '/' + this.dateStart + '/' + this.dateEnd);
            }
        });
    }
    openModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _modal_modal_page__WEBPACK_IMPORTED_MODULE_3__["ModalPage"],
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            console.log(data);
            this.status = data.status;
        });
    }
    save(contact) {
        const params = {
            name: contact.name,
            phone: contact.phone,
            email: contact.email,
        };
        this.service.create(params).subscribe(data => {
            console.log(data);
        });
    }
};
ContactsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_6__["Contacts"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _services_create_invited_service__WEBPACK_IMPORTED_MODULE_5__["CreateInvitedService"] }
];
ContactsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contacts',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contacts.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/contacts.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contacts.page.scss */ "./src/app/pages/user/menu/event/contacts/contacts.page.scss")).default]
    })
], ContactsPage);



/***/ }),

/***/ "./src/app/pages/user/menu/event/contacts/modal/modal.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/user/menu/event/contacts/modal/modal.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  --ion-background-color: #eae9e9;\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50L2NvbnRhY3RzL21vZGFsL21vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSwrQkFBQTtBQUdGOztBQURBO0VBQ0UsK0JBQUE7QUFJRjs7QUFGQTtFQUNFLCtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvbWVudS9ldmVudC9jb250YWN0cy9tb2RhbC9tb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jYXJke1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZjNmMGYwO1xufVxuaW9uLWNvbnRlbnR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxZjFmMWY7XG59XG5pb24tc2xpZGVzLCBpb24tc2xpZGV7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNlYWU5ZTk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/user/menu/event/contacts/modal/modal.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/user/menu/event/contacts/modal/modal.page.ts ***!
  \********************************************************************/
/*! exports provided: ModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPage", function() { return ModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_create_invited_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../services/create-invited.service */ "./src/app/services/create-invited.service.ts");




let ModalPage = class ModalPage {
    constructor(modalCtrl, alertController, loading, service) {
        this.modalCtrl = modalCtrl;
        this.alertController = alertController;
        this.loading = loading;
        this.service = service;
        this.status = false;
    }
    exit() {
        this.modalCtrl.dismiss({
            dismissed: true,
            status: this.status
        });
    }
    save(form) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (form.value.email === '') {
                form.value.email = 'null';
            }
            const load = yield this.alertController.create({
                message: 'Guardando...'
            });
            yield load.present();
            const params = {
                name: form.value.name,
                phone: form.value.phone,
                email: form.value.email,
            };
            this.service.create(params).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                // console.log(data);
                const alert = yield this.alertController.create({
                    header: '!Exito¡',
                    // @ts-ignore
                    message: data.message,
                    buttons: ['ok']
                });
                form.reset();
                this.status = true;
                yield load.dismiss();
                yield alert.present();
            }));
        });
    }
};
ModalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _services_create_invited_service__WEBPACK_IMPORTED_MODULE_3__["CreateInvitedService"] }
];
ModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./modal.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/modal/modal.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./modal.page.scss */ "./src/app/pages/user/menu/event/contacts/modal/modal.page.scss")).default]
    })
], ModalPage);



/***/ }),

/***/ "./src/app/services/create-invited.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/create-invited.service.ts ***!
  \****************************************************/
/*! exports provided: CreateInvitedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateInvitedService", function() { return CreateInvitedService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



let CreateInvitedService = class CreateInvitedService {
    constructor(http) {
        this.http = http;
        this.url = sessionStorage.getItem('url_global') + '/create/invited';
    }
    create(params) {
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Authorization': sessionStorage.getItem('access_token'),
        });
        return this.http.post(`${this.url}`, params, { headers });
    }
};
CreateInvitedService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CreateInvitedService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], CreateInvitedService);



/***/ })

}]);
//# sourceMappingURL=contacts-contacts-module-es2015.js.map