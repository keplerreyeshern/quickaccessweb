(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contacts-contacts-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/contacts.page.html":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/contacts.page.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserMenuEventContactsContactsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header-secondary title=\"contactos\"></app-header-secondary>\n\n<ion-content>\n    <ion-searchbar animated\n                   (ionChange)=\"search($event)\"\n                   placeholder=\"Buscar contacto\">\n    </ion-searchbar>\n    <ion-list>\n        <ion-item *ngFor=\"let contact of listContacts | filter:contactSearch\" (click)=\"select(contact)\" color=\"dark\">\n            <ion-avatar item-start>\n                <img [src]=\"contact.avatar[0].value\">\n            </ion-avatar>\n            <ion-label class=\"ion-margin-start\">{{contact.displayName }}</ion-label>\n            <ion-checkbox slot=\"end\" color=\"primary\"></ion-checkbox>\n        </ion-item>\n        <ion-item color=\"dark\">\n            <ion-avatar item-start>\n            </ion-avatar>\n            <ion-label></ion-label>\n        </ion-item>\n    </ion-list>\n    <!-- fab placed to the bottom end -->\n    <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\">\n        <ion-fab-button (click)=\"openModal()\">\n            <ion-icon name=\"add\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n    <!-- fab placed to the bottom end -->\n    <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button (click)=\"goTo()\">\n            <ion-icon name=\"arrow-forward-outline\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/modal/modal.page.html":
    /*!************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/modal/modal.page.html ***!
      \************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserMenuEventContactsModalModalPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Agregar Nuevo Invitado</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <form #form=\"ngForm\" (ngSubmit)=\"save(form)\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\">Nombre Completo</ion-label>\n            <ion-input ngModel\n                       type=\"text\"\n                       name=\"name\"\n                       required>\n            </ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Email</ion-label>\n            <ion-input type=\"email\"\n                       name=\"email\"\n                       pattern=\"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$\"\n                       ngModel>\n            </ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Numero de Celular</ion-label>\n            <ion-input type=\"number\"\n                       name=\"phone\"\n                       ngModel\n                       minlength=\"10\"\n                       maxlength=\"10\"\n                       required>\n            </ion-input>\n          </ion-item>\n          <ion-row>\n            <ion-col offset=\"1\" size=\"10\">\n              <ion-button type=\"submit\" color=\"success\" size=\"large\" [disabled]=\"form.invalid\" expand=\"block\">\n                <ion-icon name=\"save-outline\" slot=\"start\"></ion-icon>\n                Guardar\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-list>\n      </form>\n    </ion-slide>\n  </ion-slides>\n  <!-- fab placed to the bottom end -->\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"danger\" fill=\"outline\" (click)=\"exit()\">\n      <ion-icon name=\"close-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/contacts/contacts-routing.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/user/menu/event/contacts/contacts-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: ContactsPageRoutingModule */

    /***/
    function srcAppPagesUserMenuEventContactsContactsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactsPageRoutingModule", function () {
        return ContactsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _contacts_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./contacts.page */
      "./src/app/pages/user/menu/event/contacts/contacts.page.ts");

      var routes = [{
        path: '',
        component: _contacts_page__WEBPACK_IMPORTED_MODULE_3__["ContactsPage"]
      }, {
        path: 'modal',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | modal-modal-module */
          "modal-modal-module").then(__webpack_require__.bind(null,
          /*! ./modal/modal.module */
          "./src/app/pages/user/menu/event/contacts/modal/modal.module.ts")).then(function (m) {
            return m.ModalPageModule;
          });
        }
      }];

      var ContactsPageRoutingModule = function ContactsPageRoutingModule() {
        _classCallCheck(this, ContactsPageRoutingModule);
      };

      ContactsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ContactsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/contacts/contacts.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/user/menu/event/contacts/contacts.module.ts ***!
      \*******************************************************************/

    /*! exports provided: ContactsPageModule */

    /***/
    function srcAppPagesUserMenuEventContactsContactsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactsPageModule", function () {
        return ContactsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./contacts-routing.module */
      "./src/app/pages/user/menu/event/contacts/contacts-routing.module.ts");
      /* harmony import */


      var _contacts_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./contacts.page */
      "./src/app/pages/user/menu/event/contacts/contacts.page.ts");
      /* harmony import */


      var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../../components/components.module */
      "./src/app/components/components.module.ts");
      /* harmony import */


      var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../../../pipes/pipes.module */
      "./src/app/pipes/pipes.module.ts");

      var ContactsPageModule = function ContactsPageModule() {
        _classCallCheck(this, ContactsPageModule);
      };

      ContactsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactsPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]],
        declarations: [_contacts_page__WEBPACK_IMPORTED_MODULE_6__["ContactsPage"]]
      })], ContactsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/contacts/contacts.page.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/user/menu/event/contacts/contacts.page.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserMenuEventContactsContactsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-searchbar {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-slides, ion-slide {\n  height: 100%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50L2NvbnRhY3RzL2NvbnRhY3RzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSxjQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtBQUlGOztBQUZBO0VBQ0UsK0JBQUE7QUFLRjs7QUFIQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FBTUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91c2VyL21lbnUvZXZlbnQvY29udGFjdHMvY29udGFjdHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzEwMTAxMDtcbn1cbmlvbi10aXRsZXtcbiAgY29sb3I6ICNkN2Q4ZGE7XG59XG5pb24tc2VhcmNoYmFye1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jb250ZW50e1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMWYxZjFmO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tc2xpZGVzLCBpb24tc2xpZGV7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/contacts/contacts.page.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/user/menu/event/contacts/contacts.page.ts ***!
      \*****************************************************************/

    /*! exports provided: ContactsPage */

    /***/
    function srcAppPagesUserMenuEventContactsContactsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactsPage", function () {
        return ContactsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _modal_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./modal/modal.page */
      "./src/app/pages/user/menu/event/contacts/modal/modal.page.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _services_create_invited_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../../../services/create-invited.service */
      "./src/app/services/create-invited.service.ts");
      /* harmony import */


      var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/contacts/ngx */
      "./node_modules/@ionic-native/contacts/__ivy_ngcc__/ngx/index.js");

      var ContactsPage = /*#__PURE__*/function () {
        function ContactsPage(modalCtrl, router, contacts, activatedRoute, loading, alertController, service) {
          _classCallCheck(this, ContactsPage);

          this.modalCtrl = modalCtrl;
          this.router = router;
          this.contacts = contacts;
          this.activatedRoute = activatedRoute;
          this.loading = loading;
          this.alertController = alertController;
          this.service = service;
          this.listContacts = [];
          this.avatar = './assets/icon/avatar.png';
          this.status = false;
          this.inviteds = [];
          this.contactSearch = '';
        }

        _createClass(ContactsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.loadingContacts();
            this.activatedRoute.params.subscribe(function (params) {
              _this.name = params.name;
              _this.dateStart = params.dateStart;
              _this.dateEnd = params.dateEnd.substr(0, 19);
            });
          }
        }, {
          key: "loadingContacts",
          value: function loadingContacts() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this2 = this;

              var loading3;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.loading.create({
                        message: 'Cargando...'
                      });

                    case 2:
                      loading3 = _context3.sent;
                      _context3.next = 5;
                      return loading3.present();

                    case 5:
                      this.contacts.find(['*']).then(function (res) {
                        var readData = [];
                        res.map(function (item) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    readData.push({
                                      displayName: item.displayName,
                                      photos: item.photos,
                                      avatar: [{
                                        value: this.avatar
                                      }],
                                      phoneNumbers: item.phoneNumbers,
                                      emails: item.emails
                                    });

                                  case 1:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        });
                        _this2.listContacts = readData; // tslint:disable-next-line:only-arrow-functions

                        _this2.listContacts.sort(function (a, b) {
                          if (a.displayName > b.displayName) {
                            return 1;
                          }

                          if (a.displayName < b.displayName) {
                            return -1;
                          } // a must be equal to b


                          return 0;
                        });
                      }, function (error) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                            while (1) {
                              switch (_context2.prev = _context2.next) {
                                case 0:
                                  console.log(error);
                                  _context2.next = 3;
                                  return loading3.dismiss();

                                case 3:
                                case "end":
                                  return _context2.stop();
                              }
                            }
                          }, _callee2);
                        }));
                      });
                      _context3.next = 8;
                      return loading3.dismiss();

                    case 8:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "search",
          value: function search(event) {
            this.contactSearch = event.detail.value;
          }
        }, {
          key: "select",
          value: function select(contact) {
            var email;
            console.log(contact.emails); // @ts-ignore

            if (contact.emails == null) {
              email = null;
            } else {
              email = contact.emails[0].value;
            }

            var invited = {
              name: contact.displayName,
              email: email,
              phone: contact.phoneNumbers[0].value
            };
            var index;
            var verified = false;

            if (this.inviteds.length > 0) {
              for (var i = 0; i < this.inviteds.length; i++) {
                if (this.inviteds[i].name === contact.displayName) {
                  verified = true;
                  index = i;
                }
              }
            }

            if (verified) {
              this.inviteds.splice(index, 1);
            } else {
              this.inviteds.push(invited);
            }

            console.log(this.inviteds);
          }
        }, {
          key: "goTo",
          value: function goTo() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var alert, load, j, alert2;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      console.log(this.inviteds.length);
                      _context4.next = 3;
                      return this.alertController.create({
                        header: '!Error¡',
                        message: 'Debes agregar o seleccionar al menos un invitado',
                        buttons: ['ok']
                      });

                    case 3:
                      alert = _context4.sent;

                      if (!(!this.status && this.inviteds.length === 0)) {
                        _context4.next = 9;
                        break;
                      }

                      _context4.next = 7;
                      return alert.present();

                    case 7:
                      _context4.next = 23;
                      break;

                    case 9:
                      _context4.next = 11;
                      return this.alertController.create({
                        message: 'Guardando...'
                      });

                    case 11:
                      load = _context4.sent;
                      _context4.next = 14;
                      return load.present();

                    case 14:
                      // tslint:disable-next-line:prefer-for-of
                      for (j = 0; j < this.inviteds.length; j++) {
                        this.save(this.inviteds[j]);
                      }

                      _context4.next = 17;
                      return this.alertController.create({
                        header: '!Exito¡',
                        message: 'Invitados guardados con exito',
                        buttons: ['ok']
                      });

                    case 17:
                      alert2 = _context4.sent;
                      _context4.next = 20;
                      return load.dismiss();

                    case 20:
                      _context4.next = 22;
                      return alert2.present();

                    case 22:
                      this.router.navigateByUrl('/tabs/home/event-create/comments/' + this.name + '/' + this.dateStart + '/' + this.dateEnd);

                    case 23:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "openModal",
          value: function openModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var modal, _yield$modal$onWillDi, data;

              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.modalCtrl.create({
                        component: _modal_modal_page__WEBPACK_IMPORTED_MODULE_3__["ModalPage"]
                      });

                    case 2:
                      modal = _context5.sent;
                      _context5.next = 5;
                      return modal.present();

                    case 5:
                      _context5.next = 7;
                      return modal.onWillDismiss();

                    case 7:
                      _yield$modal$onWillDi = _context5.sent;
                      data = _yield$modal$onWillDi.data;
                      console.log(data);
                      this.status = data.status;

                    case 11:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "save",
          value: function save(contact) {
            var params = {
              name: contact.name,
              phone: contact.phone,
              email: contact.email
            };
            this.service.create(params).subscribe(function (data) {
              console.log(data);
            });
          }
        }]);

        return ContactsPage;
      }();

      ContactsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_6__["Contacts"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _services_create_invited_service__WEBPACK_IMPORTED_MODULE_5__["CreateInvitedService"]
        }];
      };

      ContactsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contacts',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./contacts.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/contacts.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./contacts.page.scss */
        "./src/app/pages/user/menu/event/contacts/contacts.page.scss"))["default"]]
      })], ContactsPage);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/contacts/modal/modal.page.scss":
    /*!**********************************************************************!*\
      !*** ./src/app/pages/user/menu/event/contacts/modal/modal.page.scss ***!
      \**********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserMenuEventContactsModalModalPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  --ion-background-color: #eae9e9;\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50L2NvbnRhY3RzL21vZGFsL21vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSwrQkFBQTtBQUdGOztBQURBO0VBQ0UsK0JBQUE7QUFJRjs7QUFGQTtFQUNFLCtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvbWVudS9ldmVudC9jb250YWN0cy9tb2RhbC9tb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jYXJke1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZjNmMGYwO1xufVxuaW9uLWNvbnRlbnR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxZjFmMWY7XG59XG5pb24tc2xpZGVzLCBpb24tc2xpZGV7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNlYWU5ZTk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/event/contacts/modal/modal.page.ts":
    /*!********************************************************************!*\
      !*** ./src/app/pages/user/menu/event/contacts/modal/modal.page.ts ***!
      \********************************************************************/

    /*! exports provided: ModalPage */

    /***/
    function srcAppPagesUserMenuEventContactsModalModalPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ModalPage", function () {
        return ModalPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_create_invited_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../../../services/create-invited.service */
      "./src/app/services/create-invited.service.ts");

      var ModalPage = /*#__PURE__*/function () {
        function ModalPage(modalCtrl, alertController, loading, service) {
          _classCallCheck(this, ModalPage);

          this.modalCtrl = modalCtrl;
          this.alertController = alertController;
          this.loading = loading;
          this.service = service;
          this.status = false;
        }

        _createClass(ModalPage, [{
          key: "exit",
          value: function exit() {
            this.modalCtrl.dismiss({
              dismissed: true,
              status: this.status
            });
          }
        }, {
          key: "save",
          value: function save(form) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this3 = this;

              var load, params;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      if (form.value.email === '') {
                        form.value.email = 'null';
                      }

                      _context7.next = 3;
                      return this.alertController.create({
                        message: 'Guardando...'
                      });

                    case 3:
                      load = _context7.sent;
                      _context7.next = 6;
                      return load.present();

                    case 6:
                      params = {
                        name: form.value.name,
                        phone: form.value.phone,
                        email: form.value.email
                      };
                      this.service.create(params).subscribe(function (data) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                          var alert;
                          return regeneratorRuntime.wrap(function _callee6$(_context6) {
                            while (1) {
                              switch (_context6.prev = _context6.next) {
                                case 0:
                                  _context6.next = 2;
                                  return this.alertController.create({
                                    header: '!Exito¡',
                                    // @ts-ignore
                                    message: data.message,
                                    buttons: ['ok']
                                  });

                                case 2:
                                  alert = _context6.sent;
                                  form.reset();
                                  this.status = true;
                                  _context6.next = 7;
                                  return load.dismiss();

                                case 7:
                                  _context6.next = 9;
                                  return alert.present();

                                case 9:
                                case "end":
                                  return _context6.stop();
                              }
                            }
                          }, _callee6, this);
                        }));
                      });

                    case 8:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }]);

        return ModalPage;
      }();

      ModalPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }, {
          type: _services_create_invited_service__WEBPACK_IMPORTED_MODULE_3__["CreateInvitedService"]
        }];
      };

      ModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./modal.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/contacts/modal/modal.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./modal.page.scss */
        "./src/app/pages/user/menu/event/contacts/modal/modal.page.scss"))["default"]]
      })], ModalPage);
      /***/
    },

    /***/
    "./src/app/services/create-invited.service.ts":
    /*!****************************************************!*\
      !*** ./src/app/services/create-invited.service.ts ***!
      \****************************************************/

    /*! exports provided: CreateInvitedService */

    /***/
    function srcAppServicesCreateInvitedServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CreateInvitedService", function () {
        return CreateInvitedService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var CreateInvitedService = /*#__PURE__*/function () {
        function CreateInvitedService(http) {
          _classCallCheck(this, CreateInvitedService);

          this.http = http;
          this.url = sessionStorage.getItem('url_global') + '/create/invited';
        }

        _createClass(CreateInvitedService, [{
          key: "create",
          value: function create(params) {
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Authorization': sessionStorage.getItem('access_token')
            });
            return this.http.post("".concat(this.url), params, {
              headers: headers
            });
          }
        }]);

        return CreateInvitedService;
      }();

      CreateInvitedService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      CreateInvitedService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], CreateInvitedService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=contacts-contacts-module-es5.js.map