(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-read-code-read-code-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/read-code/read-code.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/read-code/read-code.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-initial title=\"Lector de Codigo\"></app-header-initial>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <ion-button expand=\"full\"\n                  fill=\"outline\"\n                  size=\"large\"\n                  shape=\"round\"\n                  (click)=\"scan()\">\n        Escanea Codigo\n      </ion-button>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/read-code/read-code-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/read-code/read-code-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: ReadCodePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReadCodePageRoutingModule", function() { return ReadCodePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _read_code_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./read-code.page */ "./src/app/pages/read-code/read-code.page.ts");




const routes = [
    {
        path: '',
        component: _read_code_page__WEBPACK_IMPORTED_MODULE_3__["ReadCodePage"]
    },
    {
        path: 'result/:text/:format',
        loadChildren: () => __webpack_require__.e(/*! import() | result-result-module */ "result-result-module").then(__webpack_require__.bind(null, /*! ./result/result.module */ "./src/app/pages/read-code/result/result.module.ts")).then(m => m.ResultPageModule)
    }
];
let ReadCodePageRoutingModule = class ReadCodePageRoutingModule {
};
ReadCodePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ReadCodePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/read-code/read-code.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/read-code/read-code.module.ts ***!
  \*****************************************************/
/*! exports provided: ReadCodePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReadCodePageModule", function() { return ReadCodePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _read_code_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./read-code-routing.module */ "./src/app/pages/read-code/read-code-routing.module.ts");
/* harmony import */ var _read_code_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./read-code.page */ "./src/app/pages/read-code/read-code.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








let ReadCodePageModule = class ReadCodePageModule {
};
ReadCodePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _read_code_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReadCodePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_read_code_page__WEBPACK_IMPORTED_MODULE_6__["ReadCodePage"]]
    })
], ReadCodePageModule);



/***/ }),

/***/ "./src/app/pages/read-code/read-code.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/read-code/read-code.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-slides, ion-slide {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVhZC1jb2RlL3JlYWQtY29kZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBQTtBQUNGOztBQUNBO0VBQ0UsY0FBQTtBQUVGOztBQUFBO0VBQ0UsK0JBQUE7QUFHRjs7QUFEQTtFQUNFLCtCQUFBO0FBSUY7O0FBRkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQUtGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVhZC1jb2RlL3JlYWQtY29kZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jb250ZW50e1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMWYxZjFmO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tc2xpZGVzLCBpb24tc2xpZGV7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/read-code/read-code.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/read-code/read-code.page.ts ***!
  \***************************************************/
/*! exports provided: ReadCodePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReadCodePage", function() { return ReadCodePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");





let ReadCodePage = class ReadCodePage {
    constructor(barcodeScanner, router, alertController) {
        this.barcodeScanner = barcodeScanner;
        this.router = router;
        this.alertController = alertController;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.scan();
    }
    scan() {
        this.barcodeScanner.scan().then(barcodeData => {
            console.log('Barcode data', barcodeData);
            this.router.navigateByUrl('/read-code/result/' + barcodeData.text + '/' + barcodeData.format);
        }).catch((err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('Error', err);
            const alert = yield this.alertController.create({
                header: '!Error¡',
                message: err,
                buttons: ['ok']
            });
            yield alert.present();
        }));
    }
};
ReadCodePage.ctorParameters = () => [
    { type: _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_2__["BarcodeScanner"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] }
];
ReadCodePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-read-code',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./read-code.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/read-code/read-code.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./read-code.page.scss */ "./src/app/pages/read-code/read-code.page.scss")).default]
    })
], ReadCodePage);



/***/ })

}]);
//# sourceMappingURL=pages-read-code-read-code-module-es2015.js.map