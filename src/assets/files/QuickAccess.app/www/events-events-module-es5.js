(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["events-events-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/events/events.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/events/events.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserMenuEventsEventsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header-initial title=\"lista de eventos\"></app-header-initial>\n\n<ion-content ion-padding>\n  <ion-grid *ngIf=\"listActive\">\n    <ion-list-header>\n      <ion-label>Eventos Activos</ion-label>\n    </ion-list-header>\n    <ion-list>\n      <ion-item *ngFor=\"let eventActive of listActive; let index=index\" routerLink=\"details/{{eventActive.id}}\" detail>\n        <ion-label>\n          <h2>{{ eventActive.name }}</h2>\n        </ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-grid>\n  <ion-grid *ngIf=\"listDisabled\">\n    <ion-list-header>\n      <ion-label>Eventos Caducados</ion-label>\n    </ion-list-header>\n    <ion-list>\n      <ion-item *ngFor=\"let eventDisabled of listDisabled; let index=index\" routerLink=\"details/{{eventDisabled.id}}\" detail>\n        <ion-label>\n          <h2>{{ eventDisabled.name }}</h2>\n        </ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-grid>\n  <ion-grid *ngIf=\"!listActive && !listDisabled\">\n    <ion-list-header>\n      <ion-label>Aun no haz creado eventos</ion-label>\n    </ion-list-header>\n  </ion-grid>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/events/events-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/user/menu/events/events-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: EventsPageRoutingModule */

    /***/
    function srcAppPagesUserMenuEventsEventsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EventsPageRoutingModule", function () {
        return EventsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _events_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./events.page */
      "./src/app/pages/user/menu/events/events.page.ts");

      var routes = [{
        path: '',
        component: _events_page__WEBPACK_IMPORTED_MODULE_3__["EventsPage"]
      }, {
        path: 'details/:id',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | details-details-module */
          "details-details-module").then(__webpack_require__.bind(null,
          /*! ./details/details.module */
          "./src/app/pages/user/menu/events/details/details.module.ts")).then(function (m) {
            return m.DetailsPageModule;
          });
        }
      }];

      var EventsPageRoutingModule = function EventsPageRoutingModule() {
        _classCallCheck(this, EventsPageRoutingModule);
      };

      EventsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EventsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/events/events.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/user/menu/events/events.module.ts ***!
      \*********************************************************/

    /*! exports provided: EventsPageModule */

    /***/
    function srcAppPagesUserMenuEventsEventsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EventsPageModule", function () {
        return EventsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _events_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./events-routing.module */
      "./src/app/pages/user/menu/events/events-routing.module.ts");
      /* harmony import */


      var _events_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./events.page */
      "./src/app/pages/user/menu/events/events.page.ts");
      /* harmony import */


      var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../components/components.module */
      "./src/app/components/components.module.ts");

      var EventsPageModule = function EventsPageModule() {
        _classCallCheck(this, EventsPageModule);
      };

      EventsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _events_routing_module__WEBPACK_IMPORTED_MODULE_5__["EventsPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_events_page__WEBPACK_IMPORTED_MODULE_6__["EventsPage"]]
      })], EventsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/events/events.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/pages/user/menu/events/events.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserMenuEventsEventsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-label, ion-item, h2 {\n  color: #f3f0f0;\n}\n\nion-slides, ion-slide {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50cy9ldmVudHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLCtCQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtBQUlGOztBQUZBO0VBQ0UsY0FBQTtBQUtGOztBQUhBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFNRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvbWVudS9ldmVudHMvZXZlbnRzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxMDEwMTA7XG59XG5pb24tdGl0bGV7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNvbnRlbnR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxZjFmMWY7XG59XG5pb24tY2FyZHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2YzZjBmMDtcbn1cbmlvbi1sYWJlbCwgaW9uLWl0ZW0sIGgye1xuICBjb2xvcjogI2YzZjBmMDtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/events/events.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/user/menu/events/events.page.ts ***!
      \*******************************************************/

    /*! exports provided: EventsPage */

    /***/
    function srcAppPagesUserMenuEventsEventsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EventsPage", function () {
        return EventsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_events_list_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../services/events-list.service */
      "./src/app/services/events-list.service.ts");

      var EventsPage = /*#__PURE__*/function () {
        function EventsPage(loading, service) {
          _classCallCheck(this, EventsPage);

          this.loading = loading;
          this.service = service;
        }

        _createClass(EventsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.read();
          }
        }, {
          key: "read",
          value: function read() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var load;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.loading.create({
                        message: 'Descargando..'
                      });

                    case 2:
                      load = _context2.sent;
                      _context2.next = 5;
                      return load.present();

                    case 5:
                      this.service.read().subscribe(function (data) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  // @ts-ignore
                                  this.listActive = data.eventsActive; // @ts-ignore

                                  this.listDisabled = data.eventsDisable;
                                  _context.next = 4;
                                  return load.dismiss();

                                case 4:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      });

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return EventsPage;
      }();

      EventsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }, {
          type: _services_events_list_service__WEBPACK_IMPORTED_MODULE_3__["EventsListService"]
        }];
      };

      EventsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-events',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./events.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/events/events.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./events.page.scss */
        "./src/app/pages/user/menu/events/events.page.scss"))["default"]]
      })], EventsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=events-events-module-es5.js.map