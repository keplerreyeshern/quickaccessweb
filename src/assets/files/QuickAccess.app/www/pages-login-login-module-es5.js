(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title class=\"ion-text-center\">Entrar</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <form #form=\"ngForm\" >\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <ion-row>\n                <ion-col [offset]=\"4\" size=\"4\" cols=\"4\">\n                  <ion-img src=\"../../assets/images/logo.png\" ></ion-img>\n                </ion-col>\n              </ion-row>\n              <div class=\"ion-margin-top\">\n                <ion-item>\n                  <ion-label position=\"floating\">Correo</ion-label>\n                  <ion-input name=\"email\"\n                             type=\"email\"\n                             [(ngModel)]=\"email\"\n                             pattern=\"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$\"\n                             required>\n                  </ion-input>\n                </ion-item>\n                <ion-item>\n                  <ion-label  position=\"floating\">Contraseña</ion-label>\n                  <ion-input name=\"password\"\n                             [(ngModel)]=\"password\"\n                             [type]=\"type\"\n                             required>\n                  </ion-input>\n                  <button  slot=\"end\" (click)=\"togglePassword()\">\n                    <ion-icon  [name]=\"iconpassword\"></ion-icon>\n                  </button>\n                </ion-item>\n\n              </div>\n              <div class=\"ion-margin-top\">\n                <ion-button size=\"large\"\n                            type=\"button\"\n                            color=\"success\"\n                            [disabled]=\"form.invalid\"\n                            expand=\"block\"\n                            (click)=\"login(email, password)\">\n                  <ion-icon slot=\"start\" name=\"enter-outline\"></ion-icon>\n                  Entrar\n                </ion-button>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </form>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n\n";
      /***/
    },

    /***/
    "./src/app/pages/login/login-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/login/login-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppPagesLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/login/login.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/login/login.module.ts ***!
      \*********************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppPagesLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/pages/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/pages/login/login.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/pages/login/login.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  --ion-background-color: #eae9e9;\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLCtCQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBSUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jb250ZW50e1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMWYxZjFmO1xufVxuaW9uLXNsaWRlcywgaW9uLXNsaWRle1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZWFlOWU5O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/login/login.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/login/login.page.ts ***!
      \*******************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppPagesLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../services/auth.service */
      "./src/app/services/auth.service.ts");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(router, alert, loading, authService) {
          _classCallCheck(this, LoginPage);

          this.router = router;
          this.alert = alert;
          this.loading = loading;
          this.authService = authService;
          this.type = 'password';
          this.iconpassword = 'eye';

          if (sessionStorage.getItem('user_email')) {
            this.login(sessionStorage.getItem('user_email'), sessionStorage.getItem('user_password'));
          }
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "togglePassword",
          value: function togglePassword() {
            this.type = this.type === 'text' ? 'password' : 'text';
            this.iconpassword = this.iconpassword === 'eye-off' ? 'eye' : 'eye-off';
          }
        }, {
          key: "load",
          value: function load() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var load;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.loading.create({
                        message: 'Cargando...'
                      });

                    case 2:
                      load = _context2.sent;
                      _context2.next = 5;
                      return load.present();

                    case 5:
                      setTimeout(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  _context.next = 2;
                                  return load.dismiss();

                                case 2:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee);
                        }));
                      }, 1100);

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "login",
          value: function login(email, password) {
            var params = {
              grant_type: 'password',
              client_id: '91cdc214-3780-4d9e-88ad-c5f4612bf385',
              client_secret: 'b2dejALxWyEpsh8XFYtjyvOcpuFzgv4QSqRbKX6l',
              username: email,
              password: password
            };
            this.authService.postToken(params);
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/pages/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    },

    /***/
    "./src/app/services/auth.service.ts":
    /*!******************************************!*\
      !*** ./src/app/services/auth.service.ts ***!
      \******************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppServicesAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");

      var AuthService = /*#__PURE__*/function () {
        function AuthService(http, loadingController, alertController, router, storageS) {
          _classCallCheck(this, AuthService);

          this.http = http;
          this.loadingController = loadingController;
          this.alertController = alertController;
          this.router = router;
          this.storageS = storageS;
          sessionStorage.setItem('url_global', 'https://backend.quickaccess.mx/api');
          sessionStorage.setItem('url_auth', 'https://backend.quickaccess.mx/oauth/token');
          sessionStorage.setItem('url_images', 'https://backend.quickaccess.mx/storage/images');
          this.url_auth = sessionStorage.getItem('url_auth');
          this.url_global = sessionStorage.getItem('url_global');
        }

        _createClass(AuthService, [{
          key: "postToken",
          value: function postToken(params) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.loadingController.create({
                        message: 'Cargando...'
                      });

                    case 2:
                      loading = _context4.sent;
                      _context4.next = 5;
                      return loading.present();

                    case 5:
                      this.http.post("".concat(this.url_auth), params).subscribe(function (response) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                          var alertError, start, substr, end, data;
                          return regeneratorRuntime.wrap(function _callee3$(_context3) {
                            while (1) {
                              switch (_context3.prev = _context3.next) {
                                case 0:
                                  if (!response.error) {
                                    _context3.next = 10;
                                    break;
                                  }

                                  _context3.next = 3;
                                  return this.alertController.create({
                                    header: '!Error¡',
                                    // @ts-ignore
                                    message: response.message,
                                    buttons: ['ok']
                                  });

                                case 3:
                                  alertError = _context3.sent;
                                  _context3.next = 6;
                                  return alertError.present();

                                case 6:
                                  _context3.next = 8;
                                  return loading.dismiss();

                                case 8:
                                  _context3.next = 24;
                                  break;

                                case 10:
                                  if (!response.token_type) {
                                    _context3.next = 24;
                                    break;
                                  }

                                  sessionStorage.setItem("token", JSON.stringify(response));
                                  this.storage = sessionStorage.getItem('token');
                                  start = this.storage.indexOf('access_token', 0);
                                  substr = this.storage.substring(start);
                                  start = substr.indexOf(':', 0) + 2;
                                  substr = substr.substring(start);
                                  end = substr.indexOf('"', 0);
                                  this.access_token = substr.substring(0, end);
                                  sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
                                  data = this.getData(sessionStorage.getItem('access_token'), params.username, params.password);
                                  _context3.next = 23;
                                  return loading.dismiss();

                                case 23:
                                  return _context3.abrupt("return", data);

                                case 24:
                                case "end":
                                  return _context3.stop();
                              }
                            }
                          }, _callee3, this);
                        }));
                      });

                    case 6:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "getData",
          value: function getData(access_token, email, password) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this3 = this;

              var loading, headers;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.loadingController.create({
                        message: 'Cargando...'
                      });

                    case 2:
                      loading = _context6.sent;
                      _context6.next = 5;
                      return loading.present();

                    case 5:
                      headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                        'Authorization': access_token
                      });
                      this.http.get("".concat(this.url_global + '/profile'), {
                        headers: headers
                      }).subscribe(function (response) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                          var alertCompany, alertUser, alertUserAdmin, alertUserSuperAdmin;
                          return regeneratorRuntime.wrap(function _callee5$(_context5) {
                            while (1) {
                              switch (_context5.prev = _context5.next) {
                                case 0:
                                  // @ts-ignore
                                  this.user = response.user; // @ts-ignore

                                  this.company = response.company; // @ts-ignore

                                  sessionStorage.setItem('userProfile', response.user.profile); // @ts-ignore

                                  sessionStorage.setItem('userName', response.user.name); // @ts-ignore

                                  sessionStorage.setItem('userId', response.user.id); // @ts-ignore

                                  sessionStorage.setItem('change_password', response.user.password_active);

                                  if (this.company.status) {
                                    _context5.next = 15;
                                    break;
                                  }

                                  sessionStorage.clear();
                                  _context5.next = 10;
                                  return this.alertController.create({
                                    header: '!Error¡',
                                    message: 'Sin acceso a la aplicación ya que se bloqueo su empresa o residencia',
                                    buttons: ['ok']
                                  });

                                case 10:
                                  alertCompany = _context5.sent;
                                  _context5.next = 13;
                                  return alertCompany.present();

                                case 13:
                                  _context5.next = 61;
                                  break;

                                case 15:
                                  if (this.user.status) {
                                    _context5.next = 24;
                                    break;
                                  }

                                  sessionStorage.clear();
                                  _context5.next = 19;
                                  return this.alertController.create({
                                    header: '!Error¡',
                                    message: 'Sin acceso a la aplicación ya que se bloqueo su usuario',
                                    buttons: ['ok']
                                  });

                                case 19:
                                  alertUser = _context5.sent;
                                  _context5.next = 22;
                                  return alertUser.present();

                                case 22:
                                  _context5.next = 61;
                                  break;

                                case 24:
                                  if (!this.user.status) {
                                    _context5.next = 61;
                                    break;
                                  }

                                  if (!(this.user.profile == 'user')) {
                                    _context5.next = 33;
                                    break;
                                  }

                                  this.router.navigateByUrl('/user');
                                  _context5.next = 29;
                                  return loading.dismiss();

                                case 29:
                                  sessionStorage.setItem('user_email', email);
                                  sessionStorage.setItem('user_password', password);
                                  _context5.next = 61;
                                  break;

                                case 33:
                                  if (!(this.user.profile == 'vigilant')) {
                                    _context5.next = 41;
                                    break;
                                  }

                                  this.router.navigateByUrl('/read-code');
                                  _context5.next = 37;
                                  return loading.dismiss();

                                case 37:
                                  sessionStorage.setItem('user_email', email);
                                  sessionStorage.setItem('user_password', password);
                                  _context5.next = 61;
                                  break;

                                case 41:
                                  if (!(this.user.profile == 'admin')) {
                                    _context5.next = 52;
                                    break;
                                  }

                                  sessionStorage.clear();
                                  _context5.next = 45;
                                  return this.alertController.create({
                                    header: '!Error¡',
                                    message: 'Tu perfil es de administrador y no tienes acceso a la aplicación',
                                    buttons: ['ok']
                                  });

                                case 45:
                                  alertUserAdmin = _context5.sent;
                                  _context5.next = 48;
                                  return alertUserAdmin.present();

                                case 48:
                                  _context5.next = 50;
                                  return loading.dismiss();

                                case 50:
                                  _context5.next = 61;
                                  break;

                                case 52:
                                  if (!(this.user.profile == 'super_admin')) {
                                    _context5.next = 61;
                                    break;
                                  }

                                  sessionStorage.clear();
                                  _context5.next = 56;
                                  return this.alertController.create({
                                    header: '!Error¡',
                                    message: 'Tu perfil es de super administrador y no tienes acceso a la aplicación',
                                    buttons: ['ok']
                                  });

                                case 56:
                                  alertUserSuperAdmin = _context5.sent;
                                  _context5.next = 59;
                                  return alertUserSuperAdmin.present();

                                case 59:
                                  _context5.next = 61;
                                  return loading.dismiss();

                                case 61:
                                case "end":
                                  return _context5.stop();
                              }
                            }
                          }, _callee5, this);
                        }));
                      });

                    case 7:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-login-login-module-es5.js.map