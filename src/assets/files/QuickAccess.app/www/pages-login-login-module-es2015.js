(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title class=\"ion-text-center\">Entrar</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <form #form=\"ngForm\" >\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <ion-row>\n                <ion-col [offset]=\"4\" size=\"4\" cols=\"4\">\n                  <ion-img src=\"../../assets/images/logo.png\" ></ion-img>\n                </ion-col>\n              </ion-row>\n              <div class=\"ion-margin-top\">\n                <ion-item>\n                  <ion-label position=\"floating\">Correo</ion-label>\n                  <ion-input name=\"email\"\n                             type=\"email\"\n                             [(ngModel)]=\"email\"\n                             pattern=\"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$\"\n                             required>\n                  </ion-input>\n                </ion-item>\n                <ion-item>\n                  <ion-label  position=\"floating\">Contraseña</ion-label>\n                  <ion-input name=\"password\"\n                             [(ngModel)]=\"password\"\n                             [type]=\"type\"\n                             required>\n                  </ion-input>\n                  <button  slot=\"end\" (click)=\"togglePassword()\">\n                    <ion-icon  [name]=\"iconpassword\"></ion-icon>\n                  </button>\n                </ion-item>\n\n              </div>\n              <div class=\"ion-margin-top\">\n                <ion-button size=\"large\"\n                            type=\"button\"\n                            color=\"success\"\n                            [disabled]=\"form.invalid\"\n                            expand=\"block\"\n                            (click)=\"login(email, password)\">\n                  <ion-icon slot=\"start\" name=\"enter-outline\"></ion-icon>\n                  Entrar\n                </ion-button>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </form>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n\n");

/***/ }),

/***/ "./src/app/pages/login/login-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/pages/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  --ion-background-color: #eae9e9;\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLCtCQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBSUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jb250ZW50e1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMWYxZjFmO1xufVxuaW9uLXNsaWRlcywgaW9uLXNsaWRle1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZWFlOWU5O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");





let LoginPage = class LoginPage {
    constructor(router, alert, loading, authService) {
        this.router = router;
        this.alert = alert;
        this.loading = loading;
        this.authService = authService;
        this.type = 'password';
        this.iconpassword = 'eye';
        if (sessionStorage.getItem('user_email')) {
            this.login(sessionStorage.getItem('user_email'), sessionStorage.getItem('user_password'));
        }
    }
    ngOnInit() {
    }
    togglePassword() {
        this.type = this.type === 'text' ? 'password' : 'text';
        this.iconpassword = this.iconpassword === 'eye-off' ? 'eye' : 'eye-off';
    }
    load() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const load = yield this.loading.create({
                message: 'Cargando...',
            });
            yield load.present();
            setTimeout(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                yield load.dismiss();
            }), 1100);
        });
    }
    login(email, password) {
        const params = {
            grant_type: 'password',
            client_id: '91cdc214-3780-4d9e-88ad-c5f4612bf385',
            client_secret: 'b2dejALxWyEpsh8XFYtjyvOcpuFzgv4QSqRbKX6l',
            username: email,
            password,
        };
        this.authService.postToken(params);
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")).default]
    })
], LoginPage);



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");






let AuthService = class AuthService {
    constructor(http, loadingController, alertController, router, storageS) {
        this.http = http;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.router = router;
        this.storageS = storageS;
        sessionStorage.setItem('url_global', 'https://backend.quickaccess.mx/api');
        sessionStorage.setItem('url_auth', 'https://backend.quickaccess.mx/oauth/token');
        sessionStorage.setItem('url_images', 'https://backend.quickaccess.mx/storage/images');
        this.url_auth = sessionStorage.getItem('url_auth');
        this.url_global = sessionStorage.getItem('url_global');
    }
    postToken(params) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let loading = yield this.loadingController.create({
                message: 'Cargando...'
            });
            yield loading.present();
            this.http.post(`${this.url_auth}`, params).subscribe((response) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                // @ts-ignore
                if (response.error) {
                    let alertError = yield this.alertController.create({
                        header: '!Error¡',
                        // @ts-ignore
                        message: response.message,
                        buttons: ['ok']
                    });
                    yield alertError.present();
                    yield loading.dismiss();
                    // @ts-ignore
                }
                else if (response.token_type) {
                    sessionStorage.setItem("token", JSON.stringify(response));
                    this.storage = sessionStorage.getItem('token');
                    let start = this.storage.indexOf('access_token', 0);
                    let substr = this.storage.substring(start);
                    start = substr.indexOf(':', 0) + 2;
                    substr = substr.substring(start);
                    const end = substr.indexOf('"', 0);
                    this.access_token = substr.substring(0, end);
                    sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
                    let data = this.getData(sessionStorage.getItem('access_token'), params.username, params.password);
                    yield loading.dismiss();
                    return data;
                }
            }));
        });
    }
    getData(access_token, email, password) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let loading = yield this.loadingController.create({
                message: 'Cargando...'
            });
            yield loading.present();
            const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Authorization': access_token,
            });
            this.http.get(`${this.url_global + '/profile'}`, { headers }).subscribe((response) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                // @ts-ignore
                this.user = response.user;
                // @ts-ignore
                this.company = response.company;
                // @ts-ignore
                sessionStorage.setItem('userProfile', response.user.profile);
                // @ts-ignore
                sessionStorage.setItem('userName', response.user.name);
                // @ts-ignore
                sessionStorage.setItem('userId', response.user.id);
                // @ts-ignore
                sessionStorage.setItem('change_password', response.user.password_active);
                if (!this.company.status) {
                    sessionStorage.clear();
                    let alertCompany = yield this.alertController.create({
                        header: '!Error¡',
                        message: 'Sin acceso a la aplicación ya que se bloqueo su empresa o residencia',
                        buttons: ['ok']
                    });
                    yield alertCompany.present();
                }
                else {
                    if (!this.user.status) {
                        sessionStorage.clear();
                        let alertUser = yield this.alertController.create({
                            header: '!Error¡',
                            message: 'Sin acceso a la aplicación ya que se bloqueo su usuario',
                            buttons: ['ok']
                        });
                        yield alertUser.present();
                    }
                    else if (this.user.status) {
                        if (this.user.profile == 'user') {
                            this.router.navigateByUrl('/user');
                            yield loading.dismiss();
                            sessionStorage.setItem('user_email', email);
                            sessionStorage.setItem('user_password', password);
                        }
                        else if (this.user.profile == 'vigilant') {
                            this.router.navigateByUrl('/read-code');
                            yield loading.dismiss();
                            sessionStorage.setItem('user_email', email);
                            sessionStorage.setItem('user_password', password);
                        }
                        else if (this.user.profile == 'admin') {
                            sessionStorage.clear();
                            let alertUserAdmin = yield this.alertController.create({
                                header: '!Error¡',
                                message: 'Tu perfil es de administrador y no tienes acceso a la aplicación',
                                buttons: ['ok']
                            });
                            yield alertUserAdmin.present();
                            yield loading.dismiss();
                        }
                        else if (this.user.profile == 'super_admin') {
                            sessionStorage.clear();
                            let alertUserSuperAdmin = yield this.alertController.create({
                                header: '!Error¡',
                                message: 'Tu perfil es de super administrador y no tienes acceso a la aplicación',
                                buttons: ['ok']
                            });
                            yield alertUserSuperAdmin.present();
                            yield loading.dismiss();
                        }
                    }
                }
            }));
        });
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] }
];
AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthService);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map