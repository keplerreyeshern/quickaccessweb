(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["provider-provider-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.html":
    /*!********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.html ***!
      \********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserMenuProviderModalAddProviderModalAddProviderPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header-secondary title=\"agregar proveedor\"></app-header-secondary>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <ion-card>\n        <form #form=\"ngForm\" (ngSubmit)=\"save(form)\">\n          <ion-list>\n            <ion-item>\n              <ion-label position=\"floating\">Nombre Completo</ion-label>\n              <ion-input ngModel\n                         type=\"text\"\n                         name=\"name\"\n                         required>\n              </ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label position=\"floating\">Email</ion-label>\n              <ion-input type=\"email\"\n                         name=\"email\"\n                         pattern=\"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$\"\n                         ngModel>\n              </ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label position=\"floating\">Numero de Celular</ion-label>\n              <ion-input type=\"number\"\n                         name=\"phone\"\n                         ngModel\n                         minlength=\"10\"\n                         maxlength=\"10\"\n                         required>\n              </ion-input>\n            </ion-item>\n            <ion-row>\n              <ion-col offset=\"1\" size=\"10\">\n                <ion-button type=\"submit\" color=\"success\" size=\"large\" [disabled]=\"form.invalid\" expand=\"block\">\n                  <ion-icon name=\"save-outline\" slot=\"start\"></ion-icon>\n                  Guardar\n                </ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-list>\n        </form>\n      </ion-card>\n    </ion-slide>\n  </ion-slides>\n  <!-- fab placed to the bottom end -->\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"danger\" fill=\"outline\" (click)=\"exit()\">\n      <ion-icon name=\"close-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.html":
    /*!**********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.html ***!
      \**********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserMenuProviderModalListContactsModalListContactsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header-secondary title=\"Contactos\"></app-header-secondary>\n\n<ion-content>\n  <ion-searchbar animated\n                 (ionChange)=\"search($event)\"\n                 placeholder=\"Buscar contacto\">\n  </ion-searchbar>\n  <ion-list>\n    <ion-item *ngFor=\"let contact of listContacts | filter:contactSearch\" (click)=\"select(contact)\" color=\"dark\">\n      <ion-avatar item-start>\n        <img [src]=\"contact.avatar[0].value\">\n      </ion-avatar>\n      <ion-label class=\"ion-margin-start\">{{contact.displayName }}</ion-label>\n      <ion-checkbox slot=\"end\" color=\"primary\"></ion-checkbox>\n    </ion-item>\n    <ion-item color=\"dark\">\n      <ion-avatar item-start>\n      </ion-avatar>\n      <ion-label></ion-label>\n    </ion-item>\n  </ion-list>\n  <!-- fab placed to the bottom end -->\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"danger\" fill=\"outline\" (click)=\"exit()\">\n      <ion-icon name=\"close-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/provider.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/provider.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesUserMenuProviderProviderPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header-initial title=\"proveedores\"></app-header-initial>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <form #form=\"ngForm\" (ngSubmit)=\"send(form)\">\n        <ion-card>\n          <ion-card-header>\n            <ion-title color=\"dark\">Proveedor</ion-title>\n          </ion-card-header>\n          <ion-item>\n            <ion-label>Evento</ion-label>\n            <ion-select [(ngModel)]=\"idEvent\" okText=\"Aceptar\" cancelText=\"cancelar\" name=\"event\">\n              <ion-select-option  [value]=\"event.id\" *ngFor=\"let event of events; \">{{ event.name }}</ion-select-option>\n            </ion-select>\n          </ion-item>\n          <ion-item class=\"ion-margin-top ion-margin-bottom\">\n            <ion-label position=\"floating\">Fecha de Ingreso</ion-label>\n            <ion-datetime display-format=\"YYYY MMM DD h:mm a\"\n                          cancelText=\"Cancelar\"\n                          doneText=\"Aceptar\"\n                          name=\"dateStart\"\n                          pickerFormat=\"YYMMMDD HH:mm a\"\n                          [(ngModel)]=\"dateStart\"\n                          monthShortNames=\"ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic\"\n                          display-timezone=\"utc\"\n                          min=\"{{ dateNow | date: 'yyyy-MM-ddTHH:mm' }}\"\n                          max=\"2050\"\n            >\n            </ion-datetime>\n          </ion-item>\n          <ion-item class=\"ion-margin-top ion-margin-bottom\">\n            <ion-label position=\"floating\">Fecha de Salida</ion-label>\n            <ion-datetime display-format=\"YYYY MMM DD h:mm a\"\n                          cancelText=\"Cancelar\"\n                          doneText=\"Aceptar\"\n                          name=\"dateEnd\"\n                          pickerFormat=\"YYMMMDD HH:mm a\"\n                          [(ngModel)]=\"dateEnd\"\n                          monthShortNames=\"ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic\"\n                          display-timezone=\"utc\"\n                          min=\"{{ dateStart }}\"\n                          max=\"2050\"\n            >\n            </ion-datetime>\n          </ion-item>\n          <ion-button expand=\"block\" color=\"primary\" class=\"ion-margin-top\" (click)=\"goAdd()\">\n            <ion-icon slot=\"start\" name=\"person-add-outline\"></ion-icon>\n            Nuevo Proveedor\n          </ion-button>\n          <ion-button expand=\"block\" color=\"primary\" class=\"ion-margin-top\" (click)=\"goContacts()\">\n            <ion-icon slot=\"start\" name=\"people-outline\"></ion-icon>\n            Lista de Contactos\n          </ion-button>\n          <ion-button type=\"submit\"\n                      expand=\"block\"\n                      color=\"success\"\n                      class=\"ion-margin-top ion-margin-bottom\"\n                      [disabled]=\"form.invalid\">\n            <ion-icon slot=\"start\" name=\"paper-plane-outline\"></ion-icon>\n            Enviar Solicitud\n          </ion-button>\n        </ion-card>\n      </form>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.scss":
    /*!******************************************************************************************!*\
      !*** ./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.scss ***!
      \******************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserMenuProviderModalAddProviderModalAddProviderPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL21vZGFsLWFkZC1wcm92aWRlci9tb2RhbC1hZGQtcHJvdmlkZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLCtCQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtBQUlGOztBQUZBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvbWVudS9wcm92aWRlci9tb2RhbC1hZGQtcHJvdmlkZXIvbW9kYWwtYWRkLXByb3ZpZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxMDEwMTA7XG59XG5pb24tdGl0bGV7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tY29udGVudHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzFmMWYxZjtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts":
    /*!****************************************************************************************!*\
      !*** ./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts ***!
      \****************************************************************************************/

    /*! exports provided: ModalAddProviderPage */

    /***/
    function srcAppPagesUserMenuProviderModalAddProviderModalAddProviderPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ModalAddProviderPage", function () {
        return ModalAddProviderPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _services_provider_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../../../services/provider.service */
      "./src/app/services/provider.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ModalAddProviderPage = /*#__PURE__*/function () {
        function ModalAddProviderPage(modalCtrl, alertController, loading, service) {
          _classCallCheck(this, ModalAddProviderPage);

          this.modalCtrl = modalCtrl;
          this.alertController = alertController;
          this.loading = loading;
          this.service = service;
          this.status = false;
        }

        _createClass(ModalAddProviderPage, [{
          key: "exit",
          value: function exit() {
            this.modalCtrl.dismiss({
              dismissed: true,
              status: this.status
            });
          }
        }, {
          key: "save",
          value: function save(form) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var load, params;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (form.value.email === '') {
                        form.value.email = 'null';
                      }

                      _context2.next = 3;
                      return this.alertController.create({
                        message: 'Guardando...'
                      });

                    case 3:
                      load = _context2.sent;
                      _context2.next = 6;
                      return load.present();

                    case 6:
                      params = {
                        name: form.value.name,
                        phone: form.value.phone,
                        email: form.value.email,
                        house: this.house,
                        event: this.event,
                        complex: this.complex
                      };
                      this.service.create(params).subscribe(function (data) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          var alert;
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  _context.next = 2;
                                  return this.alertController.create({
                                    header: '!Exito¡',
                                    // @ts-ignore
                                    message: data.message,
                                    buttons: ['ok']
                                  });

                                case 2:
                                  alert = _context.sent;
                                  form.reset();
                                  this.status = true;
                                  _context.next = 7;
                                  return load.dismiss();

                                case 7:
                                  _context.next = 9;
                                  return alert.present();

                                case 9:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      });

                    case 8:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return ModalAddProviderPage;
      }();

      ModalAddProviderPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _services_provider_service__WEBPACK_IMPORTED_MODULE_2__["ProviderService"]
        }];
      };

      ModalAddProviderPage.propDecorators = {
        event: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      ModalAddProviderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-add-provider',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./modal-add-provider.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./modal-add-provider.page.scss */
        "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.scss"))["default"]]
      })], ModalAddProviderPage);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.scss":
    /*!********************************************************************************************!*\
      !*** ./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.scss ***!
      \********************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserMenuProviderModalListContactsModalListContactsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-searchbar {\n  color: #d7d8da;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  width: 100%;\n  height: 100%;\n}\n\nion-searchbar {\n  color: #f3f0f0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL21vZGFsLWxpc3QtY29udGFjdHMvbW9kYWwtbGlzdC1jb250YWN0cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBQTtBQUNGOztBQUNBO0VBQ0UsY0FBQTtBQUVGOztBQUFBO0VBQ0UsY0FBQTtBQUdGOztBQURBO0VBQ0UsK0JBQUE7QUFJRjs7QUFGQTtFQUNFLCtCQUFBO0FBS0Y7O0FBSEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQU1GOztBQUpBO0VBQ0UsY0FBQTtBQU9GIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL21vZGFsLWxpc3QtY29udGFjdHMvbW9kYWwtbGlzdC1jb250YWN0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1zZWFyY2hiYXJ7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tY29udGVudHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzFmMWYxZjtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbmlvbi1zZWFyY2hiYXJ7XG4gIGNvbG9yOiAjZjNmMGYwO1xufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.ts":
    /*!******************************************************************************************!*\
      !*** ./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.ts ***!
      \******************************************************************************************/

    /*! exports provided: ModalListContactsPage */

    /***/
    function srcAppPagesUserMenuProviderModalListContactsModalListContactsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ModalListContactsPage", function () {
        return ModalListContactsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_provider_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../../services/provider.service */
      "./src/app/services/provider.service.ts");
      /* harmony import */


      var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/contacts/ngx */
      "./node_modules/@ionic-native/contacts/__ivy_ngcc__/ngx/index.js");

      var ModalListContactsPage = /*#__PURE__*/function () {
        function ModalListContactsPage(modalCtrl, loading, alertController, providerService, contacts) {
          _classCallCheck(this, ModalListContactsPage);

          this.modalCtrl = modalCtrl;
          this.loading = loading;
          this.alertController = alertController;
          this.providerService = providerService;
          this.contacts = contacts;
          this.list = [];
          this.listContacts = [];
          this.avatar = './assets/icon/avatar.png';
          this.contactSearch = '';
          this.loadingContacts();
        }

        _createClass(ModalListContactsPage, [{
          key: "search",
          value: function search(event) {
            this.contactSearch = event.detail.value;
          }
        }, {
          key: "select",
          value: function select(contact) {
            var email;
            console.log(contact.emails); // @ts-ignore

            if (contact.emails == null) {
              email = null;
            } else {
              email = contact.emails[0].value;
            }

            var provider = {
              name: contact.displayName,
              email: email,
              phone: contact.phoneNumbers[0].value
            };
            var index;
            var verified = false;

            if (this.list.length > 0) {
              for (var i = 0; i < this.list.length; i++) {
                if (this.list[i].name === contact.displayName) {
                  verified = true;
                  index = i;
                }
              }
            }

            if (verified) {
              this.list.splice(index, 1);
            } else {
              this.list.push(provider);
            }

            console.log(this.list);
          }
        }, {
          key: "exit",
          value: function exit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              var load, i, params;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      if (!(this.list.length > 0)) {
                        _context4.next = 10;
                        break;
                      }

                      _context4.next = 3;
                      return this.loading.create({
                        message: 'Cargando...'
                      });

                    case 3:
                      load = _context4.sent;
                      _context4.next = 6;
                      return load.present();

                    case 6:
                      for (i = 0; i < this.list.length; i++) {
                        params = {
                          event: this.event,
                          name: this.list[i].name,
                          email: this.list[i].email,
                          telephone: this.list[i].phone
                        };
                        this.providerService.create(params).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                            var alert;
                            return regeneratorRuntime.wrap(function _callee3$(_context3) {
                              while (1) {
                                switch (_context3.prev = _context3.next) {
                                  case 0:
                                    console.log(data);
                                    _context3.next = 3;
                                    return this.alertController.create({
                                      header: '!Exito¡',
                                      // @ts-ignore
                                      message: data.message,
                                      buttons: ['ok']
                                    });

                                  case 3:
                                    alert = _context3.sent;
                                    _context3.next = 6;
                                    return alert.present();

                                  case 6:
                                  case "end":
                                    return _context3.stop();
                                }
                              }
                            }, _callee3, this);
                          }));
                        });
                      }

                      ;
                      _context4.next = 10;
                      return load.dismiss();

                    case 10:
                      this.modalCtrl.dismiss({
                        dismissed: true,
                        list: this.list
                      });

                    case 11:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "loadingContacts",
          value: function loadingContacts() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this3 = this;

              var loading3;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.loading.create({
                        message: 'Cargando...'
                      });

                    case 2:
                      loading3 = _context7.sent;
                      _context7.next = 5;
                      return loading3.present();

                    case 5:
                      this.contacts.find(['*']).then(function (res) {
                        var readData = [];
                        res.map(function (item) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                            return regeneratorRuntime.wrap(function _callee5$(_context5) {
                              while (1) {
                                switch (_context5.prev = _context5.next) {
                                  case 0:
                                    readData.push({
                                      displayName: item.displayName,
                                      photos: item.photos,
                                      avatar: [{
                                        value: this.avatar
                                      }],
                                      phoneNumbers: item.phoneNumbers,
                                      emails: item.emails
                                    });

                                  case 1:
                                  case "end":
                                    return _context5.stop();
                                }
                              }
                            }, _callee5, this);
                          }));
                        });
                        _this3.listContacts = readData; // tslint:disable-next-line:only-arrow-functions

                        _this3.listContacts.sort(function (a, b) {
                          if (a.displayName > b.displayName) {
                            return 1;
                          }

                          if (a.displayName < b.displayName) {
                            return -1;
                          } // a must be equal to b


                          return 0;
                        });
                      }, function (error) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                          return regeneratorRuntime.wrap(function _callee6$(_context6) {
                            while (1) {
                              switch (_context6.prev = _context6.next) {
                                case 0:
                                  console.log(error);
                                  _context6.next = 3;
                                  return loading3.dismiss();

                                case 3:
                                case "end":
                                  return _context6.stop();
                              }
                            }
                          }, _callee6);
                        }));
                      });
                      _context7.next = 8;
                      return loading3.dismiss();

                    case 8:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }]);

        return ModalListContactsPage;
      }();

      ModalListContactsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _services_provider_service__WEBPACK_IMPORTED_MODULE_3__["ProviderService"]
        }, {
          type: _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_4__["Contacts"]
        }];
      };

      ModalListContactsPage.propDecorators = {
        event: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      ModalListContactsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-list-contacts',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./modal-list-contacts.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./modal-list-contacts.page.scss */
        "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.scss"))["default"]]
      })], ModalListContactsPage);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/provider-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/user/menu/provider/provider-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: ProviderPageRoutingModule */

    /***/
    function srcAppPagesUserMenuProviderProviderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProviderPageRoutingModule", function () {
        return ProviderPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _provider_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./provider.page */
      "./src/app/pages/user/menu/provider/provider.page.ts");

      var routes = [{
        path: '',
        component: _provider_page__WEBPACK_IMPORTED_MODULE_3__["ProviderPage"]
      }, {
        path: 'modal-list-contacts',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | modal-list-contacts-modal-list-contacts-module */
          "modal-list-contacts-modal-list-contacts-module").then(__webpack_require__.bind(null,
          /*! ./modal-list-contacts/modal-list-contacts.module */
          "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.module.ts")).then(function (m) {
            return m.ModalListContactsPageModule;
          });
        }
      }, {
        path: 'modal-add-provider',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | modal-add-provider-modal-add-provider-module */
          "modal-add-provider-modal-add-provider-module").then(__webpack_require__.bind(null,
          /*! ./modal-add-provider/modal-add-provider.module */
          "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.module.ts")).then(function (m) {
            return m.ModalAddProviderPageModule;
          });
        }
      }];

      var ProviderPageRoutingModule = function ProviderPageRoutingModule() {
        _classCallCheck(this, ProviderPageRoutingModule);
      };

      ProviderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProviderPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/provider.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/user/menu/provider/provider.module.ts ***!
      \*************************************************************/

    /*! exports provided: ProviderPageModule */

    /***/
    function srcAppPagesUserMenuProviderProviderModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProviderPageModule", function () {
        return ProviderPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _provider_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./provider-routing.module */
      "./src/app/pages/user/menu/provider/provider-routing.module.ts");
      /* harmony import */


      var _provider_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./provider.page */
      "./src/app/pages/user/menu/provider/provider.page.ts");
      /* harmony import */


      var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../components/components.module */
      "./src/app/components/components.module.ts");

      var ProviderPageModule = function ProviderPageModule() {
        _classCallCheck(this, ProviderPageModule);
      };

      ProviderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _provider_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProviderPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_provider_page__WEBPACK_IMPORTED_MODULE_6__["ProviderPage"]]
      })], ProviderPageModule);
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/provider.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/pages/user/menu/provider/provider.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesUserMenuProviderProviderPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-label {\n  color: #d7d8da;\n}\n\nion-item {\n  padding: 1rem 0;\n}\n\nion-slides, ion-slide {\n  --ion-background-color: #eae9e9;\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL3Byb3ZpZGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSwrQkFBQTtBQUdGOztBQURBO0VBQ0UsY0FBQTtBQUlGOztBQUZBO0VBQ0UsZUFBQTtBQUtGOztBQUhBO0VBQ0UsK0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQU1GIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL3Byb3ZpZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxMDEwMTA7XG59XG5pb24tdGl0bGV7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNvbnRlbnR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxZjFmMWY7XG59XG5pb24tbGFiZWx7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWl0ZW17XG4gIHBhZGRpbmc6IDFyZW0gMDtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2VhZTllOTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/user/menu/provider/provider.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/pages/user/menu/provider/provider.page.ts ***!
      \***********************************************************/

    /*! exports provided: ProviderPage */

    /***/
    function srcAppPagesUserMenuProviderProviderPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProviderPage", function () {
        return ProviderPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_events_list_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../services/events-list.service */
      "./src/app/services/events-list.service.ts");
      /* harmony import */


      var _services_provider_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../services/provider.service */
      "./src/app/services/provider.service.ts");
      /* harmony import */


      var _modal_list_contacts_modal_list_contacts_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./modal-list-contacts/modal-list-contacts.page */
      "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.ts");
      /* harmony import */


      var _modal_add_provider_modal_add_provider_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./modal-add-provider/modal-add-provider.page */
      "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts");

      var ProviderPage = /*#__PURE__*/function () {
        function ProviderPage(modalCtrl, loadingCtrl, alertCtrl, serviceEvents, serviceProvider) {
          _classCallCheck(this, ProviderPage);

          this.modalCtrl = modalCtrl;
          this.loadingCtrl = loadingCtrl;
          this.alertCtrl = alertCtrl;
          this.serviceEvents = serviceEvents;
          this.serviceProvider = serviceProvider;
          this.contact = [];
          this.contacts = [];
          this.events = [];
          this.dateNow = new Date();
        }

        _createClass(ProviderPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var _this4 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.next = 2;
                      return this.loadingCtrl.create({
                        message: 'Cargando..'
                      });

                    case 2:
                      loading = _context9.sent;
                      _context9.next = 5;
                      return loading.present();

                    case 5:
                      this.serviceEvents.read().subscribe(function (data) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                          return regeneratorRuntime.wrap(function _callee8$(_context8) {
                            while (1) {
                              switch (_context8.prev = _context8.next) {
                                case 0:
                                  // @ts-ignore
                                  this.events = data.eventsActive;
                                  _context8.next = 3;
                                  return loading.dismiss();

                                case 3:
                                case "end":
                                  return _context8.stop();
                              }
                            }
                          }, _callee8, this);
                        }));
                      });

                    case 6:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "goContacts",
          value: function goContacts() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
              var modal, alert, _yield$modal$onWillDi, data;

              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                while (1) {
                  switch (_context10.prev = _context10.next) {
                    case 0:
                      _context10.next = 2;
                      return this.modalCtrl.create({
                        component: _modal_list_contacts_modal_list_contacts_page__WEBPACK_IMPORTED_MODULE_5__["ModalListContactsPage"],
                        componentProps: {
                          event: this.idEvent
                        }
                      });

                    case 2:
                      modal = _context10.sent;
                      _context10.next = 5;
                      return this.alertCtrl.create({
                        header: '!Error¡',
                        message: 'Debes agregar o seleccionar un evento',
                        buttons: ['ok']
                      });

                    case 5:
                      alert = _context10.sent;

                      if (!(this.idEvent == null)) {
                        _context10.next = 11;
                        break;
                      }

                      _context10.next = 9;
                      return alert.present();

                    case 9:
                      _context10.next = 13;
                      break;

                    case 11:
                      _context10.next = 13;
                      return modal.present();

                    case 13:
                      _context10.next = 15;
                      return modal.onWillDismiss();

                    case 15:
                      _yield$modal$onWillDi = _context10.sent;
                      data = _yield$modal$onWillDi.data;
                      console.log(data);
                      this.contacts = data.list;

                    case 19:
                    case "end":
                      return _context10.stop();
                  }
                }
              }, _callee10, this);
            }));
          }
        }, {
          key: "goAdd",
          value: function goAdd() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
              var modal, alert, _yield$modal$onWillDi2, data;

              return regeneratorRuntime.wrap(function _callee11$(_context11) {
                while (1) {
                  switch (_context11.prev = _context11.next) {
                    case 0:
                      _context11.next = 2;
                      return this.modalCtrl.create({
                        component: _modal_add_provider_modal_add_provider_page__WEBPACK_IMPORTED_MODULE_6__["ModalAddProviderPage"],
                        componentProps: {
                          event: this.idEvent
                        }
                      });

                    case 2:
                      modal = _context11.sent;
                      _context11.next = 5;
                      return this.alertCtrl.create({
                        header: '!Error¡',
                        message: 'Debes agregar o seleccionar un evento',
                        buttons: ['ok']
                      });

                    case 5:
                      alert = _context11.sent;

                      if (!(this.idEvent == null)) {
                        _context11.next = 11;
                        break;
                      }

                      _context11.next = 9;
                      return alert.present();

                    case 9:
                      _context11.next = 13;
                      break;

                    case 11:
                      _context11.next = 13;
                      return modal.present();

                    case 13:
                      _context11.next = 15;
                      return modal.onWillDismiss();

                    case 15:
                      _yield$modal$onWillDi2 = _context11.sent;
                      data = _yield$modal$onWillDi2.data;
                      console.log(data);
                      this.status = data.status;

                    case 19:
                    case "end":
                      return _context11.stop();
                  }
                }
              }, _callee11, this);
            }));
          }
        }, {
          key: "send",
          value: function send(form) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
              var _this5 = this;

              var loading2, alert;
              return regeneratorRuntime.wrap(function _callee13$(_context13) {
                while (1) {
                  switch (_context13.prev = _context13.next) {
                    case 0:
                      _context13.next = 2;
                      return this.loadingCtrl.create({
                        message: 'Cargando...'
                      });

                    case 2:
                      loading2 = _context13.sent;
                      _context13.next = 5;
                      return loading2.present();

                    case 5:
                      console.log(this.contacts.length);
                      _context13.next = 8;
                      return this.alertCtrl.create({
                        header: '!Error¡',
                        message: 'Debes agregar o seleccionar al menos un proveedor',
                        buttons: ['ok']
                      });

                    case 8:
                      alert = _context13.sent;

                      if (!(!this.status && this.contacts.length === 0)) {
                        _context13.next = 14;
                        break;
                      }

                      _context13.next = 12;
                      return alert.present();

                    case 12:
                      _context13.next = 15;
                      break;

                    case 14:
                      this.serviceProvider.confirm(form.value, this.idEvent).subscribe(function (data) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
                          var alert2;
                          return regeneratorRuntime.wrap(function _callee12$(_context12) {
                            while (1) {
                              switch (_context12.prev = _context12.next) {
                                case 0:
                                  console.log(data);
                                  _context12.next = 3;
                                  return this.alertCtrl.create({
                                    header: '!Exitó¡',
                                    // @ts-ignore
                                    message: data.message,
                                    buttons: ['ok']
                                  });

                                case 3:
                                  alert2 = _context12.sent;
                                  _context12.next = 6;
                                  return loading2.dismiss();

                                case 6:
                                  _context12.next = 8;
                                  return alert2.present();

                                case 8:
                                case "end":
                                  return _context12.stop();
                              }
                            }
                          }, _callee12, this);
                        }));
                      });

                    case 15:
                    case "end":
                      return _context13.stop();
                  }
                }
              }, _callee13, this);
            }));
          }
        }]);

        return ProviderPage;
      }();

      ProviderPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _services_events_list_service__WEBPACK_IMPORTED_MODULE_3__["EventsListService"]
        }, {
          type: _services_provider_service__WEBPACK_IMPORTED_MODULE_4__["ProviderService"]
        }];
      };

      ProviderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-provider',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./provider.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/provider.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./provider.page.scss */
        "./src/app/pages/user/menu/provider/provider.page.scss"))["default"]]
      })], ProviderPage);
      /***/
    },

    /***/
    "./src/app/services/provider.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/services/provider.service.ts ***!
      \**********************************************/

    /*! exports provided: ProviderService */

    /***/
    function srcAppServicesProviderServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProviderService", function () {
        return ProviderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var ProviderService = /*#__PURE__*/function () {
        function ProviderService(http) {
          _classCallCheck(this, ProviderService);

          this.http = http;
          this.url = sessionStorage.getItem('url_global') + '/create/provider';
          this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Authorization': sessionStorage.getItem('access_token')
          });
        }

        _createClass(ProviderService, [{
          key: "create",
          value: function create(params) {
            return this.http.post("".concat(this.url), params, {
              headers: this.headers
            });
          }
        }, {
          key: "confirm",
          value: function confirm(form, event) {
            var params = {
              event: event,
              entry: form.dateStart,
              exit: form.dateEnd
            };
            return this.http.post("".concat(this.url + '/confirm'), params, {
              headers: this.headers
            });
          }
        }]);

        return ProviderService;
      }();

      ProviderService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      ProviderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ProviderService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=provider-provider-module-es5.js.map