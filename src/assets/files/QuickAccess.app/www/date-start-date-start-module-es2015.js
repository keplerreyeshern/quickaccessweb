(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["date-start-date-start-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/date-start/date-start.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/date-start/date-start.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-secondary title=\"fecha de inicio\"></app-header-secondary>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <ion-card>\n        <ion-card-header class=\"ion-margin-top ion-margin-bottom\">\n          <ion-card-subtitle class=\"ion-text-center\">¿Cuando Inicia el Evento?</ion-card-subtitle>\n        </ion-card-header>\n\n        <ion-card-content>\n          <ion-item class=\"ion-margin-top ion-margin-bottom\">\n            <ion-label position=\"floating\">Fecha de Inicio</ion-label>\n            <ion-datetime display-format=\"DD MMM YYYY h:mm a\"\n                          cancelText=\"Cancelar\"\n                          doneText=\"Aceptar\"\n                          [(ngModel)]=\"date\"\n                          pickerFormat=\"YYMMMDD h:mm a\"\n                          display-timezone=\"utc\"\n                          monthShortNames=\"ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic\"\n                          min=\"{{ now | date: 'yyyy-MM-ddTHH:mm' }}\"\n                          max=\"2050\"\n            >\n            </ion-datetime>\n          </ion-item>\n          <ion-row class=\"ion-margin-top ion-margin-bottom\">\n            <ion-col offset=\"2\" size=\"8\">\n              <ion-button expand=\"block\" color=\"success\" (click)=\"goTo()\">\n                Siguiente\n                <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-card-content>\n      </ion-card>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/user/menu/event/date-start/date-start-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/user/menu/event/date-start/date-start-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: DateStartPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateStartPageRoutingModule", function() { return DateStartPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _date_start_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./date-start.page */ "./src/app/pages/user/menu/event/date-start/date-start.page.ts");




const routes = [
    {
        path: '',
        component: _date_start_page__WEBPACK_IMPORTED_MODULE_3__["DateStartPage"]
    }
];
let DateStartPageRoutingModule = class DateStartPageRoutingModule {
};
DateStartPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DateStartPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/menu/event/date-start/date-start.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/user/menu/event/date-start/date-start.module.ts ***!
  \***********************************************************************/
/*! exports provided: DateStartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateStartPageModule", function() { return DateStartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _date_start_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./date-start-routing.module */ "./src/app/pages/user/menu/event/date-start/date-start-routing.module.ts");
/* harmony import */ var _date_start_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./date-start.page */ "./src/app/pages/user/menu/event/date-start/date-start.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../components/components.module */ "./src/app/components/components.module.ts");








let DateStartPageModule = class DateStartPageModule {
};
DateStartPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _date_start_routing_module__WEBPACK_IMPORTED_MODULE_5__["DateStartPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_date_start_page__WEBPACK_IMPORTED_MODULE_6__["DateStartPage"]]
    })
], DateStartPageModule);



/***/ }),

/***/ "./src/app/pages/user/menu/event/date-start/date-start.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/user/menu/event/date-start/date-start.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-slides, ion-slide {\n  height: 100%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50L2RhdGUtc3RhcnQvZGF0ZS1zdGFydC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBQTtBQUNGOztBQUNBO0VBQ0UsY0FBQTtBQUVGOztBQUFBO0VBQ0UsK0JBQUE7QUFHRjs7QUFEQTtFQUNFLCtCQUFBO0FBSUY7O0FBRkE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtBQUtGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci9tZW51L2V2ZW50L2RhdGUtc3RhcnQvZGF0ZS1zdGFydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1jb250ZW50e1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMWYxZjFmO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tc2xpZGVzLCBpb24tc2xpZGV7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/user/menu/event/date-start/date-start.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/user/menu/event/date-start/date-start.page.ts ***!
  \*********************************************************************/
/*! exports provided: DateStartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateStartPage", function() { return DateStartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let DateStartPage = class DateStartPage {
    constructor(activatedRouter, alertController, loading, router) {
        this.activatedRouter = activatedRouter;
        this.alertController = alertController;
        this.loading = loading;
        this.router = router;
        this.date = new Date();
        this.now = new Date();
    }
    ngOnInit() {
        this.activatedRouter.params.subscribe(params => {
            // console.log(params['name']);
            this.name = params.name;
        });
    }
    changeDate(event) {
        console.log('ionChange', event);
        console.log(this.date);
    }
    goTo() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: '!Error¡',
                message: 'La fecha debe ser mayor a la fecha de hoy',
                buttons: ['ok']
            });
            if (this.now > this.date) {
                yield alert.present();
            }
            else {
                yield this.router.navigateByUrl('/user/menu/event/date-end/' + this.name + '/' + this.date);
            }
        });
    }
};
DateStartPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
DateStartPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-date-start',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./date-start.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/event/date-start/date-start.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./date-start.page.scss */ "./src/app/pages/user/menu/event/date-start/date-start.page.scss")).default]
    })
], DateStartPage);



/***/ })

}]);
//# sourceMappingURL=date-start-date-start-module-es2015.js.map