(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["provider-provider-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-secondary title=\"agregar proveedor\"></app-header-secondary>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <ion-card>\n        <form #form=\"ngForm\" (ngSubmit)=\"save(form)\">\n          <ion-list>\n            <ion-item>\n              <ion-label position=\"floating\">Nombre Completo</ion-label>\n              <ion-input ngModel\n                         type=\"text\"\n                         name=\"name\"\n                         required>\n              </ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label position=\"floating\">Email</ion-label>\n              <ion-input type=\"email\"\n                         name=\"email\"\n                         pattern=\"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$\"\n                         ngModel>\n              </ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label position=\"floating\">Numero de Celular</ion-label>\n              <ion-input type=\"number\"\n                         name=\"phone\"\n                         ngModel\n                         minlength=\"10\"\n                         maxlength=\"10\"\n                         required>\n              </ion-input>\n            </ion-item>\n            <ion-row>\n              <ion-col offset=\"1\" size=\"10\">\n                <ion-button type=\"submit\" color=\"success\" size=\"large\" [disabled]=\"form.invalid\" expand=\"block\">\n                  <ion-icon name=\"save-outline\" slot=\"start\"></ion-icon>\n                  Guardar\n                </ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-list>\n        </form>\n      </ion-card>\n    </ion-slide>\n  </ion-slides>\n  <!-- fab placed to the bottom end -->\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"danger\" fill=\"outline\" (click)=\"exit()\">\n      <ion-icon name=\"close-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-secondary title=\"Contactos\"></app-header-secondary>\n\n<ion-content>\n  <ion-searchbar animated\n                 (ionChange)=\"search($event)\"\n                 placeholder=\"Buscar contacto\">\n  </ion-searchbar>\n  <ion-list>\n    <ion-item *ngFor=\"let contact of listContacts | filter:contactSearch\" (click)=\"select(contact)\" color=\"dark\">\n      <ion-avatar item-start>\n        <img [src]=\"contact.avatar[0].value\">\n      </ion-avatar>\n      <ion-label class=\"ion-margin-start\">{{contact.displayName }}</ion-label>\n      <ion-checkbox slot=\"end\" color=\"primary\"></ion-checkbox>\n    </ion-item>\n    <ion-item color=\"dark\">\n      <ion-avatar item-start>\n      </ion-avatar>\n      <ion-label></ion-label>\n    </ion-item>\n  </ion-list>\n  <!-- fab placed to the bottom end -->\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"danger\" fill=\"outline\" (click)=\"exit()\">\n      <ion-icon name=\"close-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/provider.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/provider.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-initial title=\"proveedores\"></app-header-initial>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <form #form=\"ngForm\" (ngSubmit)=\"send(form)\">\n        <ion-card>\n          <ion-card-header>\n            <ion-title color=\"dark\">Proveedor</ion-title>\n          </ion-card-header>\n          <ion-item>\n            <ion-label>Evento</ion-label>\n            <ion-select [(ngModel)]=\"idEvent\" okText=\"Aceptar\" cancelText=\"cancelar\" name=\"event\">\n              <ion-select-option  [value]=\"event.id\" *ngFor=\"let event of events; \">{{ event.name }}</ion-select-option>\n            </ion-select>\n          </ion-item>\n          <ion-item class=\"ion-margin-top ion-margin-bottom\">\n            <ion-label position=\"floating\">Fecha de Ingreso</ion-label>\n            <ion-datetime display-format=\"YYYY MMM DD h:mm a\"\n                          cancelText=\"Cancelar\"\n                          doneText=\"Aceptar\"\n                          name=\"dateStart\"\n                          pickerFormat=\"YYMMMDD HH:mm a\"\n                          [(ngModel)]=\"dateStart\"\n                          monthShortNames=\"ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic\"\n                          display-timezone=\"utc\"\n                          min=\"{{ dateNow | date: 'yyyy-MM-ddTHH:mm' }}\"\n                          max=\"2050\"\n            >\n            </ion-datetime>\n          </ion-item>\n          <ion-item class=\"ion-margin-top ion-margin-bottom\">\n            <ion-label position=\"floating\">Fecha de Salida</ion-label>\n            <ion-datetime display-format=\"YYYY MMM DD h:mm a\"\n                          cancelText=\"Cancelar\"\n                          doneText=\"Aceptar\"\n                          name=\"dateEnd\"\n                          pickerFormat=\"YYMMMDD HH:mm a\"\n                          [(ngModel)]=\"dateEnd\"\n                          monthShortNames=\"ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic\"\n                          display-timezone=\"utc\"\n                          min=\"{{ dateStart }}\"\n                          max=\"2050\"\n            >\n            </ion-datetime>\n          </ion-item>\n          <ion-button expand=\"block\" color=\"primary\" class=\"ion-margin-top\" (click)=\"goAdd()\">\n            <ion-icon slot=\"start\" name=\"person-add-outline\"></ion-icon>\n            Nuevo Proveedor\n          </ion-button>\n          <ion-button expand=\"block\" color=\"primary\" class=\"ion-margin-top\" (click)=\"goContacts()\">\n            <ion-icon slot=\"start\" name=\"people-outline\"></ion-icon>\n            Lista de Contactos\n          </ion-button>\n          <ion-button type=\"submit\"\n                      expand=\"block\"\n                      color=\"success\"\n                      class=\"ion-margin-top ion-margin-bottom\"\n                      [disabled]=\"form.invalid\">\n            <ion-icon slot=\"start\" name=\"paper-plane-outline\"></ion-icon>\n            Enviar Solicitud\n          </ion-button>\n        </ion-card>\n      </form>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL21vZGFsLWFkZC1wcm92aWRlci9tb2RhbC1hZGQtcHJvdmlkZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLCtCQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtBQUlGOztBQUZBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvbWVudS9wcm92aWRlci9tb2RhbC1hZGQtcHJvdmlkZXIvbW9kYWwtYWRkLXByb3ZpZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxMDEwMTA7XG59XG5pb24tdGl0bGV7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tY29udGVudHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzFmMWYxZjtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts ***!
  \****************************************************************************************/
/*! exports provided: ModalAddProviderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalAddProviderPage", function() { return ModalAddProviderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_provider_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/provider.service */ "./src/app/services/provider.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let ModalAddProviderPage = class ModalAddProviderPage {
    constructor(modalCtrl, alertController, loading, service) {
        this.modalCtrl = modalCtrl;
        this.alertController = alertController;
        this.loading = loading;
        this.service = service;
        this.status = false;
    }
    exit() {
        this.modalCtrl.dismiss({
            dismissed: true,
            status: this.status,
        });
    }
    save(form) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (form.value.email === '') {
                form.value.email = 'null';
            }
            const load = yield this.alertController.create({
                message: 'Guardando...'
            });
            yield load.present();
            const params = {
                name: form.value.name,
                phone: form.value.phone,
                email: form.value.email,
                house: this.house,
                event: this.event,
                complex: this.complex,
            };
            this.service.create(params).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                // console.log(data);
                const alert = yield this.alertController.create({
                    header: '!Exito¡',
                    // @ts-ignore
                    message: data.message,
                    buttons: ['ok']
                });
                form.reset();
                this.status = true;
                yield load.dismiss();
                yield alert.present();
            }));
        });
    }
};
ModalAddProviderPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _services_provider_service__WEBPACK_IMPORTED_MODULE_2__["ProviderService"] }
];
ModalAddProviderPage.propDecorators = {
    event: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
ModalAddProviderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-add-provider',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./modal-add-provider.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./modal-add-provider.page.scss */ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.scss")).default]
    })
], ModalAddProviderPage);



/***/ }),

/***/ "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-searchbar {\n  color: #d7d8da;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-slides, ion-slide {\n  width: 100%;\n  height: 100%;\n}\n\nion-searchbar {\n  color: #f3f0f0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL21vZGFsLWxpc3QtY29udGFjdHMvbW9kYWwtbGlzdC1jb250YWN0cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBQTtBQUNGOztBQUNBO0VBQ0UsY0FBQTtBQUVGOztBQUFBO0VBQ0UsY0FBQTtBQUdGOztBQURBO0VBQ0UsK0JBQUE7QUFJRjs7QUFGQTtFQUNFLCtCQUFBO0FBS0Y7O0FBSEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQU1GOztBQUpBO0VBQ0UsY0FBQTtBQU9GIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL21vZGFsLWxpc3QtY29udGFjdHMvbW9kYWwtbGlzdC1jb250YWN0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMDEwO1xufVxuaW9uLXRpdGxle1xuICBjb2xvcjogI2Q3ZDhkYTtcbn1cbmlvbi1zZWFyY2hiYXJ7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNhcmR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmM2YwZjA7XG59XG5pb24tY29udGVudHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzFmMWYxZjtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbmlvbi1zZWFyY2hiYXJ7XG4gIGNvbG9yOiAjZjNmMGYwO1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.ts ***!
  \******************************************************************************************/
/*! exports provided: ModalListContactsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalListContactsPage", function() { return ModalListContactsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_provider_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/provider.service */ "./src/app/services/provider.service.ts");
/* harmony import */ var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/contacts/ngx */ "./node_modules/@ionic-native/contacts/__ivy_ngcc__/ngx/index.js");





let ModalListContactsPage = class ModalListContactsPage {
    constructor(modalCtrl, loading, alertController, providerService, contacts) {
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.alertController = alertController;
        this.providerService = providerService;
        this.contacts = contacts;
        this.list = [];
        this.listContacts = [];
        this.avatar = './assets/icon/avatar.png';
        this.contactSearch = '';
        this.loadingContacts();
    }
    search(event) {
        this.contactSearch = event.detail.value;
    }
    select(contact) {
        let email;
        console.log(contact.emails);
        // @ts-ignore
        if (contact.emails == null) {
            email = null;
        }
        else {
            email = contact.emails[0].value;
        }
        const provider = {
            name: contact.displayName,
            email,
            phone: contact.phoneNumbers[0].value
        };
        let index;
        let verified = false;
        if (this.list.length > 0) {
            for (let i = 0; i < this.list.length; i++) {
                if (this.list[i].name === contact.displayName) {
                    verified = true;
                    index = i;
                }
            }
        }
        if (verified) {
            this.list.splice(index, 1);
        }
        else {
            this.list.push(provider);
        }
        console.log(this.list);
    }
    exit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.list.length > 0) {
                const load = yield this.loading.create({
                    message: 'Cargando...'
                });
                yield load.present();
                for (let i = 0; i < this.list.length; i++) {
                    const params = {
                        event: this.event,
                        name: this.list[i].name,
                        email: this.list[i].email,
                        telephone: this.list[i].phone,
                    };
                    this.providerService.create(params).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        console.log(data);
                        const alert = yield this.alertController.create({
                            header: '!Exito¡',
                            // @ts-ignore
                            message: data.message,
                            buttons: ['ok']
                        });
                        yield alert.present();
                    }));
                }
                ;
                yield load.dismiss();
            }
            this.modalCtrl.dismiss({
                dismissed: true,
                list: this.list,
            });
        });
    }
    loadingContacts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading3 = yield this.loading.create({
                message: 'Cargando...'
            });
            yield loading3.present();
            this.contacts.find(['*'])
                .then(res => {
                const readData = [];
                res.map((item) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    readData.push({
                        displayName: item.displayName,
                        photos: item.photos,
                        avatar: [{ value: this.avatar }],
                        phoneNumbers: item.phoneNumbers,
                        emails: item.emails,
                    });
                }));
                this.listContacts = readData;
                // tslint:disable-next-line:only-arrow-functions
                this.listContacts.sort(function (a, b) {
                    if (a.displayName > b.displayName) {
                        return 1;
                    }
                    if (a.displayName < b.displayName) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }, (error) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                console.log(error);
                yield loading3.dismiss();
            }));
            yield loading3.dismiss();
        });
    }
};
ModalListContactsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _services_provider_service__WEBPACK_IMPORTED_MODULE_3__["ProviderService"] },
    { type: _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_4__["Contacts"] }
];
ModalListContactsPage.propDecorators = {
    event: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
ModalListContactsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-list-contacts',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./modal-list-contacts.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./modal-list-contacts.page.scss */ "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.scss")).default]
    })
], ModalListContactsPage);



/***/ }),

/***/ "./src/app/pages/user/menu/provider/provider-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/provider-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ProviderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProviderPageRoutingModule", function() { return ProviderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _provider_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./provider.page */ "./src/app/pages/user/menu/provider/provider.page.ts");




const routes = [
    {
        path: '',
        component: _provider_page__WEBPACK_IMPORTED_MODULE_3__["ProviderPage"]
    },
    {
        path: 'modal-list-contacts',
        loadChildren: () => __webpack_require__.e(/*! import() | modal-list-contacts-modal-list-contacts-module */ "modal-list-contacts-modal-list-contacts-module").then(__webpack_require__.bind(null, /*! ./modal-list-contacts/modal-list-contacts.module */ "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.module.ts")).then(m => m.ModalListContactsPageModule)
    },
    {
        path: 'modal-add-provider',
        loadChildren: () => __webpack_require__.e(/*! import() | modal-add-provider-modal-add-provider-module */ "modal-add-provider-modal-add-provider-module").then(__webpack_require__.bind(null, /*! ./modal-add-provider/modal-add-provider.module */ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.module.ts")).then(m => m.ModalAddProviderPageModule)
    }
];
let ProviderPageRoutingModule = class ProviderPageRoutingModule {
};
ProviderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProviderPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/menu/provider/provider.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/provider.module.ts ***!
  \*************************************************************/
/*! exports provided: ProviderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProviderPageModule", function() { return ProviderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _provider_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./provider-routing.module */ "./src/app/pages/user/menu/provider/provider-routing.module.ts");
/* harmony import */ var _provider_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./provider.page */ "./src/app/pages/user/menu/provider/provider.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../components/components.module */ "./src/app/components/components.module.ts");








let ProviderPageModule = class ProviderPageModule {
};
ProviderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _provider_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProviderPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_provider_page__WEBPACK_IMPORTED_MODULE_6__["ProviderPage"]]
    })
], ProviderPageModule);



/***/ }),

/***/ "./src/app/pages/user/menu/provider/provider.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/user/menu/provider/provider.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-label {\n  color: #d7d8da;\n}\n\nion-item {\n  padding: 1rem 0;\n}\n\nion-slides, ion-slide {\n  --ion-background-color: #eae9e9;\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL3Byb3ZpZGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSwrQkFBQTtBQUdGOztBQURBO0VBQ0UsY0FBQTtBQUlGOztBQUZBO0VBQ0UsZUFBQTtBQUtGOztBQUhBO0VBQ0UsK0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQU1GIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci9tZW51L3Byb3ZpZGVyL3Byb3ZpZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxMDEwMTA7XG59XG5pb24tdGl0bGV7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNvbnRlbnR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxZjFmMWY7XG59XG5pb24tbGFiZWx7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWl0ZW17XG4gIHBhZGRpbmc6IDFyZW0gMDtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2VhZTllOTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/user/menu/provider/provider.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/user/menu/provider/provider.page.ts ***!
  \***********************************************************/
/*! exports provided: ProviderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProviderPage", function() { return ProviderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_events_list_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/events-list.service */ "./src/app/services/events-list.service.ts");
/* harmony import */ var _services_provider_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/provider.service */ "./src/app/services/provider.service.ts");
/* harmony import */ var _modal_list_contacts_modal_list_contacts_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modal-list-contacts/modal-list-contacts.page */ "./src/app/pages/user/menu/provider/modal-list-contacts/modal-list-contacts.page.ts");
/* harmony import */ var _modal_add_provider_modal_add_provider_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal-add-provider/modal-add-provider.page */ "./src/app/pages/user/menu/provider/modal-add-provider/modal-add-provider.page.ts");







let ProviderPage = class ProviderPage {
    constructor(modalCtrl, loadingCtrl, alertCtrl, serviceEvents, serviceProvider) {
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.serviceEvents = serviceEvents;
        this.serviceProvider = serviceProvider;
        this.contact = [];
        this.contacts = [];
        this.events = [];
        this.dateNow = new Date();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando..'
            });
            yield loading.present();
            this.serviceEvents.read().subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                // @ts-ignore
                this.events = data.eventsActive;
                yield loading.dismiss();
            }));
        });
    }
    goContacts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _modal_list_contacts_modal_list_contacts_page__WEBPACK_IMPORTED_MODULE_5__["ModalListContactsPage"],
                componentProps: {
                    event: this.idEvent
                }
            });
            const alert = yield this.alertCtrl.create({
                header: '!Error¡',
                message: 'Debes agregar o seleccionar un evento',
                buttons: ['ok']
            });
            if (this.idEvent == null) {
                yield alert.present();
            }
            else {
                yield modal.present();
            }
            const { data } = yield modal.onWillDismiss();
            console.log(data);
            this.contacts = data.list;
        });
    }
    goAdd() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _modal_add_provider_modal_add_provider_page__WEBPACK_IMPORTED_MODULE_6__["ModalAddProviderPage"],
                componentProps: {
                    event: this.idEvent
                }
            });
            const alert = yield this.alertCtrl.create({
                header: '!Error¡',
                message: 'Debes agregar o seleccionar un evento',
                buttons: ['ok']
            });
            if (this.idEvent == null) {
                yield alert.present();
            }
            else {
                yield modal.present();
            }
            const { data } = yield modal.onWillDismiss();
            console.log(data);
            this.status = data.status;
        });
    }
    send(form) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading2 = yield this.loadingCtrl.create({
                message: 'Cargando...'
            });
            yield loading2.present();
            console.log(this.contacts.length);
            const alert = yield this.alertCtrl.create({
                header: '!Error¡',
                message: 'Debes agregar o seleccionar al menos un proveedor',
                buttons: ['ok']
            });
            if (!this.status && this.contacts.length === 0) {
                yield alert.present();
            }
            else {
                this.serviceProvider.confirm(form.value, this.idEvent).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    console.log(data);
                    const alert2 = yield this.alertCtrl.create({
                        header: '!Exitó¡',
                        // @ts-ignore
                        message: data.message,
                        buttons: ['ok']
                    });
                    yield loading2.dismiss();
                    yield alert2.present();
                }));
            }
        });
    }
};
ProviderPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _services_events_list_service__WEBPACK_IMPORTED_MODULE_3__["EventsListService"] },
    { type: _services_provider_service__WEBPACK_IMPORTED_MODULE_4__["ProviderService"] }
];
ProviderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-provider',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./provider.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/menu/provider/provider.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./provider.page.scss */ "./src/app/pages/user/menu/provider/provider.page.scss")).default]
    })
], ProviderPage);



/***/ }),

/***/ "./src/app/services/provider.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/provider.service.ts ***!
  \**********************************************/
/*! exports provided: ProviderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProviderService", function() { return ProviderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



let ProviderService = class ProviderService {
    constructor(http) {
        this.http = http;
        this.url = sessionStorage.getItem('url_global') + '/create/provider';
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Authorization': sessionStorage.getItem('access_token'),
        });
    }
    create(params) {
        return this.http.post(`${this.url}`, params, { headers: this.headers });
    }
    confirm(form, event) {
        const params = {
            event,
            entry: form.dateStart,
            exit: form.dateEnd
        };
        return this.http.post(`${this.url + '/confirm'}`, params, { headers: this.headers });
    }
};
ProviderService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ProviderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ProviderService);



/***/ })

}]);
//# sourceMappingURL=provider-provider-module-es2015.js.map