(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["result-result-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/read-code/result/result.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/read-code/result/result.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header-secondary title=\"resultado del codigo\"></app-header-secondary>\n\n<ion-content>\n  <ion-slides>\n    <ion-slide>\n      <ion-card>\n        <ion-card-header>\n          <ion-card-title>¡Hola {{ subject }}!</ion-card-title>\n        </ion-card-header>\n        <ion-card-content>\n          <p>{{ message }}</p>\n          <p>{{ date }}</p>\n        </ion-card-content>\n      </ion-card>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/read-code/result/result-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/read-code/result/result-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ResultPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultPageRoutingModule", function() { return ResultPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _result_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./result.page */ "./src/app/pages/read-code/result/result.page.ts");




const routes = [
    {
        path: '',
        component: _result_page__WEBPACK_IMPORTED_MODULE_3__["ResultPage"]
    }
];
let ResultPageRoutingModule = class ResultPageRoutingModule {
};
ResultPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ResultPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/read-code/result/result.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/read-code/result/result.module.ts ***!
  \*********************************************************/
/*! exports provided: ResultPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultPageModule", function() { return ResultPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _result_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./result-routing.module */ "./src/app/pages/read-code/result/result-routing.module.ts");
/* harmony import */ var _result_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./result.page */ "./src/app/pages/read-code/result/result.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let ResultPageModule = class ResultPageModule {
};
ResultPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _result_routing_module__WEBPACK_IMPORTED_MODULE_5__["ResultPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_result_page__WEBPACK_IMPORTED_MODULE_6__["ResultPage"]]
    })
], ResultPageModule);



/***/ }),

/***/ "./src/app/pages/read-code/result/result.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/read-code/result/result.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  --ion-background-color: #101010;\n}\n\nion-title {\n  color: #d7d8da;\n}\n\nion-content {\n  --ion-background-color: #1f1f1f;\n}\n\nion-card {\n  --ion-background-color: #f3f0f0;\n}\n\nion-slides, ion-slide {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVhZC1jb2RlL3Jlc3VsdC9yZXN1bHQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLCtCQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtBQUlGOztBQUZBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlYWQtY29kZS9yZXN1bHQvcmVzdWx0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxMDEwMTA7XG59XG5pb24tdGl0bGV7XG4gIGNvbG9yOiAjZDdkOGRhO1xufVxuaW9uLWNvbnRlbnR7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxZjFmMWY7XG59XG5pb24tY2FyZHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2YzZjBmMDtcbn1cbmlvbi1zbGlkZXMsIGlvbi1zbGlkZXtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/read-code/result/result.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/read-code/result/result.page.ts ***!
  \*******************************************************/
/*! exports provided: ResultPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultPage", function() { return ResultPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_result_code_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/result-code.service */ "./src/app/services/result-code.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




let ResultPage = class ResultPage {
    constructor(activatedRoute, service) {
        this.activatedRoute = activatedRoute;
        this.service = service;
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.text = params.text;
            this.format = params.format;
            this.user = params.user;
            // console.log(this.text);
            // console.log(this.format);
            this.read();
        });
    }
    read() {
        if (this.format === 'CODE_QR') {
            const subjectV = this.text.substr(0, this.text.indexOf('-'));
            const complet = this.text.substr(this.text.indexOf('-') + 1);
            if (subjectV === 'Invited') {
                const invited = complet.substr(0, complet.indexOf('-'));
                const event = complet.substr(complet.indexOf('-') + 1);
                const params = {
                    invited,
                    event,
                    user: this.user,
                };
                this.service.invited(params).subscribe(res => {
                    // @ts-ignore
                    this.subject = res.invited;
                    // @ts-ignore
                    this.message = res.message;
                    // @ts-ignore
                    this.date = res.date;
                });
            }
            else if (subjectV === 'Provider') {
                const provider = complet.substr(0, complet.indexOf('-'));
                const event = complet.substr(complet.indexOf('-') + 1);
                const params = {
                    provider,
                    event,
                    user: this.user,
                };
                this.service.provider(params).subscribe(res => {
                    // @ts-ignore
                    this.subject = res.provider;
                    // @ts-ignore
                    this.message = res.message;
                    // @ts-ignore
                    this.date = res.date;
                });
            }
            else if (subjectV === 'Settler') {
                const settler = complet.substr(0, complet.indexOf('-'));
                const params = {
                    settler,
                    user: this.user,
                };
                this.service.settler(params).subscribe(res => {
                    // @ts-ignore
                    this.subject = res.settler;
                    // @ts-ignore
                    this.message = res.message;
                    // @ts-ignore
                    this.date = res.date;
                });
            }
            else {
                this.subject = 'Anonimo';
                this.message = 'el codigo no es de esta aplicación verifica y vuelve a intentar';
                this.date = '';
            }
        }
        else {
            this.subject = 'Anonimo';
            this.message = 'el codigo no es QR ni tampoco de esta aplicación verifica y vuelve a intentar';
            this.date = '';
        }
    }
};
ResultPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_result_code_service__WEBPACK_IMPORTED_MODULE_2__["ResultCodeService"] }
];
ResultPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-result',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./result.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/read-code/result/result.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./result.page.scss */ "./src/app/pages/read-code/result/result.page.scss")).default]
    })
], ResultPage);



/***/ }),

/***/ "./src/app/services/result-code.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/result-code.service.ts ***!
  \*************************************************/
/*! exports provided: ResultCodeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultCodeService", function() { return ResultCodeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



let ResultCodeService = class ResultCodeService {
    constructor(http) {
        this.http = http;
        this.url = sessionStorage.getItem('url_global');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Authorization': sessionStorage.getItem('access_token'),
        });
    }
    invited(params) {
        return this.http.post(`${this.url + '/invited'}`, params);
    }
    provider(params) {
        return this.http.post(`${this.url + '/provider'}`, params);
    }
    settler(params) {
        return this.http.post(`${this.url + '/settler'}`, params);
    }
};
ResultCodeService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ResultCodeService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ResultCodeService);



/***/ })

}]);
//# sourceMappingURL=result-result-module-es2015.js.map